package ru.majo.bsutimetable;

import android.content.Context;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Majo on 04.07.2015.
 */
public class TimetableLessonsAdapter extends RecyclerView.Adapter<TimetableLessonsAdapter.ViewHolder> {


    private Context mContext;
    private ArrayList<MainTimetable> mLessons = new ArrayList<>();
    private Boolean mAnimate = true;

    public TimetableLessonsAdapter(ArrayList<MainTimetable> lessons,Context context){
        mLessons = lessons;
        mContext = context;
    }

    public TimetableLessonsAdapter(ArrayList<MainTimetable> lessons,Context context,boolean animate){
        mLessons = lessons;
        mContext = context;
        mAnimate = animate;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView typeTextView;
        TextView descrTextView;
        TextView teacherTextView;
        TextView timeTextView;
        TextView audTextView;
        ImageView imageView;
        LinearLayout lessonLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            lessonLayout = (LinearLayout)itemView.findViewById(R.id.lesson_layout);
            typeTextView = (TextView)itemView.findViewById(R.id.type_lesson_textview);
            descrTextView = (TextView)itemView.findViewById(R.id.descrption_lesson_textview);
            teacherTextView = (TextView)itemView.findViewById(R.id.teacher_lesson_textview);
            timeTextView = (TextView)itemView.findViewById(R.id.time_lesson_textview);
            audTextView = (TextView)itemView.findViewById(R.id.auditory_lesson_textview);
            imageView = (ImageView)itemView.findViewById(R.id.type_lesson_color_imageview);
        }
    }
    @Override
    public TimetableLessonsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_timetable_lessons,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TimetableLessonsAdapter.ViewHolder holder, int position) {
        holder.lessonLayout.setVisibility(View.INVISIBLE);
        mLessons.get(position);
        holder.typeTextView.setText(mLessons.get(position).getType());
        holder.descrTextView.setText(mLessons.get(position).getDiss());
        holder.teacherTextView.setText(mLessons.get(position).getFirstType());
        holder.timeTextView.setText(mLessons.get(position).getTime());
        holder.audTextView.setText(mLessons.get(position).getAuditory());
        setType(mLessons.get(position).getType(), holder);
        if (mAnimate)
            animateItem(holder.lessonLayout,position);
        else
            holder.lessonLayout.setVisibility(View.VISIBLE);

    }

    private void animateItem(View viewToAnimate, int position){
        viewToAnimate.setAlpha(0);

        viewToAnimate.animate().alpha(1).setDuration((position + 1) * 100).setInterpolator(new FastOutLinearInInterpolator()).setStartDelay(300).start();//.translationY(0).setDuration((position + 1) * 100).setInterpolator(new AccelerateInterpolator()).setStartDelay(position*100).start();
        viewToAnimate.setVisibility(View.VISIBLE);
    }

    private void setType(String type,TimetableLessonsAdapter.ViewHolder vh){
        if (type.equals("ЛБ"))
            vh.imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.shape_circle));
        else if (type.equals("ЛК"))
            vh.imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.shape_circle_purple));
        else if (type.equals("ПР"))
            vh.imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.shape_circle_yellow));
        else if (type.equals("ЗЧ"))
            vh.imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.shape_circle_green));
        else if (type.equals("ЭК"))
            vh.imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.shape_circle_orange));


    }

    @Override
    public int getItemCount() {
        return mLessons.size();
    }
}
