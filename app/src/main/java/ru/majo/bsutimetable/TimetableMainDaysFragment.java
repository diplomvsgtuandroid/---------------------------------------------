package ru.majo.bsutimetable;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Majo on 13.09.2015.
 */
public class TimetableMainDaysFragment extends Fragment {

    private DBReader mDBReader;
    TabLayout tabLayout;
    ViewPager viewPager;

    private int mType;
    private int mWeek;
    private String mValue;


    public static TimetableMainDaysFragment newInstance(int type,String value,int week){
        TimetableMainDaysFragment timetableTabsFragment = new TimetableMainDaysFragment();
        Bundle bundle = new Bundle(3);
        bundle.putInt("type", type);
        bundle.putInt("week", week);
        bundle.putString("value", value);
        timetableTabsFragment.setArguments(bundle);
        return timetableTabsFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_timetable_main_weeks,null);
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        Log.e("ONCRETE", "TimetableMainDaysFragment");

        mDBReader = new DBReader(getActivity());
        mType = getArguments().getInt("type");
        mWeek = getArguments().getInt("week");
        mValue = getArguments().getString("value");

        setupViewPager(viewPager);
        tabLayout = (TabLayout) view.findViewById(R.id.sliding_tabs);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.setupWithViewPager(viewPager);
        return view;
    }
    private void setCurrent(){
        if (mWeek==Utils.getParity(Utils.newDate()) && Utils.getToday()!=7) {
            viewPager.setCurrentItem(Utils.getToday()-1);
        }
    }

    private void setupViewPager(final ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        for (int i=0;i<6;i++){
            adapter.addFrag(TimetableMainLessonsFragment.newInstance(mType, mValue, mWeek, i), Utils.getShortDay(i));
        }
        viewPager.setAdapter(adapter);

    }


    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private List<Fragment> mFragmentList;
        private List<String> mFragmentTitleList;
        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
            mFragmentList = new ArrayList<>();
            mFragmentTitleList = new ArrayList<>();
        }
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }
        @Override
        public int getCount() {
            return mFragmentList.size();
        }
        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

    }
}
