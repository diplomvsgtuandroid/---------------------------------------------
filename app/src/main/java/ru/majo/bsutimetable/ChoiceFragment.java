package ru.majo.bsutimetable;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Majo on 30.08.2015.
 */
public class ChoiceFragment extends Fragment {

    private SharedPreferences mSP;

    private Spinner mSpinner;
    private AutoCompleteTextView mAutoCompleteTextView;
    private Button mButtonSave;

    private int mPosition = -1;
    private ArrayList<String> mList = new ArrayList<String>();
    private DBReader mDBReader;
    private String[] mTypes = {"Очные группы","Преподаватели","Заочные группы","Полная занятость преподавателей","Циклы",
        "Полная занятость аудиторий"};

    private CallbackListener mListener;

    public static ChoiceFragment newInstance(){
        ChoiceFragment choiceFragment = new ChoiceFragment();
        return choiceFragment;
    }

    public interface CallbackListener{
        public void onChange();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mListener = (CallbackListener)activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_choice,container,false);
        mSP = getActivity().getSharedPreferences("settings", Context.MODE_PRIVATE);
        mSpinner = (Spinner)view.findViewById(R.id.spiner_choice);
        mAutoCompleteTextView = (AutoCompleteTextView)view.findViewById(R.id.autocompletetextview_choice);
        mButtonSave = (Button)view.findViewById(R.id.button_save_choice);
        Toast.makeText(getActivity(), "Если ваша группа/фамилия отстуствует, то попробуйте обновить расписание",
                Toast.LENGTH_LONG).show();
        mDBReader = new DBReader(getActivity());

        setAdapterToAuto(0);
        mSpinner.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, mTypes));
        mSpinner.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(
                            AdapterView<?> parent, View view, int position, long id) {
                        mPosition = position;
                        setAdapterToAuto(position);
                    }

                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
        mButtonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkInputCorrectness(mAutoCompleteTextView.getText().toString(),mPosition)) {
                    getFragmentManager().popBackStack();
                    getFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            TimetableTabsFragment.newInstance(mSP.getInt("type_save", 0),
                                    mSP.getString("value_save", new String()))).commit();
                    mListener.onChange();

                }
                else
                    Toast.makeText(getActivity(),"Проверьте правильность ввода",Toast.LENGTH_SHORT).show();
            }
        });
        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (getView() == null)
            return;
        View view = getView().findFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private boolean checkInputCorrectness(String input,int type){
        if (containInList(input,mList)){
            if (type==1 || type==3)
                input = mDBReader.convertTeacher(input);
            saveValues(input,type);
            return true;
        }
        return false;
    }

    private boolean containInList(String find,ArrayList<String> array){
        for (int i=0;i<array.size();i++){
            if (find.equals(array.get(i))){
                return true;
            }
        }
        return false;
    }

    private void saveValues(String value,int type){
        SharedPreferences.Editor e = mSP.edit();
        e.putString("value_save", value);
        e.putInt("type_save",type);
        e.apply();
    }

    private void setAdapterToAuto(int position) {
        mList = reahHintsList(position);
        ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, mList);
        mAutoCompleteTextView.setAdapter(stringArrayAdapter);
    }

    private ArrayList<String> reahHintsList(int i){
        return mDBReader.readListForHints(i);
    }
}
