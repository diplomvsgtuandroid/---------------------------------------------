package ru.majo.bsutimetable;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimetableWidget extends AppWidgetProvider {
	
    final String LOG_TAG = "My Logs";
    private SharedPreferences mSettings;
    int widgetID;
    private DBReader mDBReader;
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private String mValue;
    private int mTypeValue;

    public void onEnabled(Context context) {
          super.onEnabled(context);
          Log.d(LOG_TAG, "onEnabled");

      }
    @SuppressWarnings("deprecation")
      @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
        int[] appWidgetIds) {

        mSettings = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        mDBReader = new DBReader(context);

        mTypeValue = mSettings.getInt("type_save", -1);
        mValue = mSettings.getString("value_save", "");

        final int N = appWidgetIds.length;
        for (int i = 0; i < N; ++i) {
            DBReader dbr = new DBReader(context);
            Intent intent = new Intent(context,ServiceWidget.class);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetIds[i]);
            RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.widget_timetable);
            if (mTypeValue>-1 && mTypeValue<2){
                String value = mValue;
                rv.setTextViewText(R.id.date_widget, Utils.getParity(dateFormat.format(new Date())) + " неделя");
                rv.setTextViewText(R.id.day_widget, Utils.getDayMain(Utils.getToday()));
                if (mTypeValue==1)
                    value = dbr.convertbackTeacher(value);
                rv.setTextViewText(R.id.value_widget,value);
            }
            else if (mTypeValue>1 && mTypeValue<6) {
                String value = mValue;
                rv.setTextViewText(R.id.date_widget, Utils.getDayAndMonth(dateFormat.format(new Date())));
                rv.setTextViewText(R.id.day_widget, Utils.getDayMain(Utils.getToday()));
                if (mTypeValue==3)
                    value = dbr.convertbackTeacher(value);
                rv.setTextViewText(R.id.value_widget,value);
            }
            if (isEmptyList(mTypeValue,mValue,mDBReader)) {
                rv.setViewVisibility(R.id.widget_textview, View.VISIBLE);
                rv.setViewVisibility(R.id.widget_recyclerview, View.GONE);
                Log.e("VISIBIILITY","VISIBLE");
            }else {
                rv.setRemoteAdapter(appWidgetIds[i], R.id.widget_recyclerview, intent);
            }
            setClick(rv, context, appWidgetIds[i]);
            appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds[i], R.id.widget_recyclerview);
            appWidgetManager.updateAppWidget(appWidgetIds[i], rv);
        }
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    private boolean isEmptyList(int type,String value,DBReader dbr){
        if (type>-1) {
            if (type<2){
                if (dbr.readMain(String.valueOf(type),String.valueOf(Utils.getToday()),
                        String.valueOf(Utils.getParity(dateFormat.format(new Date()))),value).size()==0)
                    return true;
                else
                    return false;
            }
            else{
                if (dbr.readDmain(String.valueOf(type),value,dateFormat.format(new Date())).size()==0)
                    return true;
                else
                    return false;
            }
        }
        return true;
    }

    void setClick(RemoteViews rv, Context context, int appWidgetId) {
        Intent ClickIntent = new Intent(context, TimetableWidget.class);
        ClickIntent.setAction("ACTION_ON_CLICK");
        Log.e("CLICKS", "setClick");

        PendingIntent actionPendingIntent = PendingIntent.getBroadcast(context, 0, ClickIntent, 0);
        rv.setOnClickPendingIntent(R.id.layout_widget, actionPendingIntent);

    }
    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        Log.e("CLICKS","CLICKRECIVER");
        if (intent.getAction().equals("ACTION_ON_CLICK")) {
            Intent i = new Intent(context,MainActivity.class);
            //i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP );
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }

    }

}
