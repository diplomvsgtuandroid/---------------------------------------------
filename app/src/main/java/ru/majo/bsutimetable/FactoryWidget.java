package ru.majo.bsutimetable;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService.RemoteViewsFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class FactoryWidget implements RemoteViewsFactory {

	private ArrayList<MainTimetable> lessons;
    private Context mContext;
    int widgetID;
    private DateFormat dateFormat;
    private DBReader mDbr;
	private SharedPreferences mSettings;

	FactoryWidget(Context ctx,Intent intent){
        mContext = ctx;
		readDB();
		Log.i("WIDGET_LIST", "CONSTRUCTOR");
	    widgetID = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
	            AppWidgetManager.INVALID_APPWIDGET_ID);
	}
	
	void readDB(){
	    dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        mDbr = new DBReader(mContext);
        mSettings = mContext.getSharedPreferences("settings", Context.MODE_PRIVATE);
		int type = mSettings.getInt("type_save", -1);
		String input = mSettings.getString("value_save", new String());
		lessons = new ArrayList<>();
        if (type>-1) {
			if (type<2){
				lessons = mDbr.readMain(String.valueOf(type),String.valueOf(Utils.getToday()),
						String.valueOf(Utils.getParity(dateFormat.format(new Date()))),input);
			}
			else{
				lessons = mDbr.readDmain(String.valueOf(type),input,dateFormat.format(new Date()));
			}
        }
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		readDB();
		Log.i("WIDGET_LIST", "MNBV");
		
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return lessons.size();
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public RemoteViews getLoadingView() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RemoteViews getViewAt(int arg0) {
		// TODO Auto-generated method stub
		Log.i("WIDGET_LIST", String.valueOf(arg0));
		RemoteViews rView = new RemoteViews(mContext.getPackageName(), R.layout.item_widget_timetable);
		rView.setTextViewText(R.id.type_lesson_textview,lessons.get(arg0).getType());
		rView.setTextViewText(R.id.descrption_lesson_textview,lessons.get(arg0).getDiss());
		rView.setTextViewText(R.id.teacher_lesson_textview,lessons.get(arg0).getFirstType());
		rView.setTextViewText(R.id.time_lesson_textview,lessons.get(arg0).getTime());
		rView.setTextViewText(R.id.auditory_lesson_textview, lessons.get(arg0).getAuditory());

		return rView;
	}

	private void setType(String type,RemoteViews view){
		Log.e("TYPES",type);



	}

	@Override
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return true;
	}

	

	@Override
	public void onDataSetChanged() {
		// TODO Auto-generated method stub
		readDB();
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		
	}

}
