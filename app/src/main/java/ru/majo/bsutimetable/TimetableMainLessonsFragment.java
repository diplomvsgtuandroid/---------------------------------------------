package ru.majo.bsutimetable;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * Created by Majo on 04.07.2015.
 */
public class TimetableMainLessonsFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private DBReader mDbReader;

    private TextView mEmptyListTextView;

    public static TimetableMainLessonsFragment newInstance(int type,String value,int week, int day){
        TimetableMainLessonsFragment timetableLessonsFragment = new TimetableMainLessonsFragment();
        Bundle bundle = new Bundle(4);
        bundle.putInt("type", type);
        bundle.putString("value", value);
        bundle.putInt("week", week);
        bundle.putInt("day", day + 1);
        timetableLessonsFragment.setArguments(bundle);
        return timetableLessonsFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        Log.e("ONCRETE", "TimetableMainLessonsFragment");

        View view = inflater.inflate(R.layout.fragment_timetable_main_lessons,null);

        mRecyclerView = (RecyclerView)view.findViewById(R.id.lessons_recyclerview);
        mEmptyListTextView = (TextView)view.findViewById(R.id.lessons_textview);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mDbReader = new DBReader(getActivity());

        ArrayList<MainTimetable> lessons;
        if (getArguments().getInt("type")<2) {
            Log.e("LESSONS", String.valueOf(getArguments().getInt("day")) + " " + String.valueOf(getArguments().getInt("week")));
            lessons = mDbReader.readMain(String.valueOf(getArguments().getInt("type")), String.valueOf(getArguments().getInt("day")),
                    String.valueOf(getArguments().getInt("week")), getArguments().getString("value"));
        }
        else {
            Log.d("LESSONS",String.valueOf(getArguments().getInt("type")) + getArguments().getString("value") +
                    getArguments().getString("date"));
            lessons = mDbReader.readDmain(String.valueOf(getArguments().getInt("type")), getArguments().getString("value"),
                    getArguments().getString("date"));
        }
        if (lessons.isEmpty()) {
            mRecyclerView.setVisibility(View.GONE);
            mEmptyListTextView.setVisibility(View.VISIBLE);
        }
        else {
            mAdapter = new TimetableLessonsAdapter(lessons, getActivity(),false);
            mRecyclerView.setAdapter(mAdapter);
        }
        return view;
    }
}
