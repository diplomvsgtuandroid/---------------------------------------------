package ru.majo.bsutimetable;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by Majo on 02.07.2015.
 */
public class TimetableDaysFragment extends Fragment{
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;

    public static TimetableDaysFragment newInstance(int type,String value,int week,boolean isCurrent){
        TimetableDaysFragment timetableDaysFragment = new TimetableDaysFragment();
        Bundle bundle = new Bundle(3);
        bundle.putInt("week", week);
        bundle.putString("value", value);
        bundle.putInt("type", type);
        bundle.putBoolean("isCurrent", isCurrent);
        timetableDaysFragment.setArguments(bundle);

        return timetableDaysFragment;
    }

    public static TimetableDaysFragment newInstance(int type,String value,ArrayList<String> list,boolean isCurrent){
        TimetableDaysFragment timetableDaysFragment = new TimetableDaysFragment();
        Bundle bundle = new Bundle(3);
        bundle.putStringArrayList("list", list);
        bundle.putString("value", value);
        bundle.putInt("type", type);
        bundle.putBoolean("isCurrent",isCurrent);
        timetableDaysFragment.setArguments(bundle);

        return timetableDaysFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_timetable_days,null);
        mRecyclerView = (RecyclerView)view.findViewById(R.id.days_recyclerview);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        DBReader dbReader = new DBReader(getActivity());
        TextView toolbar = (TextView) getActivity().findViewById(R.id.toolbar_text);
        String value = getArguments().getString("value");
        if (getArguments().getInt("type")==1 || getArguments().getInt("type")==3)
            value = dbReader.convertbackTeacher(value);
        toolbar.setText(value);
        if (getArguments().getInt("type")<2) {
            mRecyclerView.setAdapter(new TimetableDaysAdapter(getActivity(),getArguments().getInt("type"),
                    getArguments().getString("value"),getArguments().getInt("week"),getArguments().getBoolean("isCurrent")));
        } else{
            mRecyclerView.setAdapter(new TimetableDaysAdapter(getActivity(),getArguments().getInt("type"),
                    getArguments().getString("value"),getArguments().getStringArrayList("list"),getArguments().getBoolean("isCurrent")));
        }
        return view;
    }
}
