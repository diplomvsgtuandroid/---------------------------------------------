package ru.majo.bsutimetable;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class DBReader {	
	DBHelper sqh;
	SQLiteDatabase sqdb;

    public DBReader(Context ctx){
		sqh = new DBHelper(ctx);
		sqdb = sqh.getReadableDatabase();
	}

	//countGroup
	public int readCountGroup(){
		String empty = "SELECT COUNT(*) FROM "+DBHelper.TABLE_GROUP;
		Cursor emp=sqdb.rawQuery(empty, null);
		emp.moveToFirst();
		return emp.getInt(0);
	}
	//countSubjects
	public int readCountSubjects(){
		String empty = "SELECT COUNT(*) FROM "+DBHelper.TABLE_SUBJECTS;
		Cursor emp=sqdb.rawQuery(empty, null);
		emp.moveToFirst();
		return emp.getInt(0);
	}
	//countTeacher
	public int readCountTeacher(){
		String empty = "SELECT COUNT(*) FROM "+DBHelper.TABLE_TEACHERS;
		Cursor emp=sqdb.rawQuery(empty, null);
		emp.moveToFirst();
		return emp.getInt(0);
	}
	//countMain
	public int readCountMain(){
		String empty = "SELECT COUNT(*) FROM "+DBHelper.TABLE_MAIN;
		Cursor emp=sqdb.rawQuery(empty, null);
		emp.moveToFirst();
		return emp.getInt(0);
	}
	//countDmain
	public int readCountDmain(){
		String empty = "SELECT COUNT(*) FROM "+DBHelper.TABLE_DMAIN;
		Cursor emp=sqdb.rawQuery(empty, null);
		emp.moveToFirst();
		return emp.getInt(0);
	}
	//countAuditory
		public int readCountAuditory(){
			String empty = "SELECT COUNT(*) FROM "+DBHelper.TABLE_AUDITORY;
			Cursor emp=sqdb.rawQuery(empty, null);
			emp.moveToFirst();
			return emp.getInt(0);
		}

	//covnert
	public String convertTeacher(String input){
		String empty = "SELECT "+ DBHelper.PREP_TEACHERS +" FROM "+DBHelper.TABLE_TEACHERS + " WHERE " + DBHelper.FIO_TEACHERS + "='" +input+"'";
		Cursor emp=sqdb.rawQuery(empty, null);
		emp.moveToFirst();
		return emp.getString(emp.getColumnIndex(DBHelper.PREP_TEACHERS));
	}
	////////back_convert
	public String convertbackTeacher(String input){
		if (!input.equals("0")){
			Log.e("INUPTS",input);
            String empty = "SELECT "+ DBHelper.FIO_TEACHERS +" FROM "+DBHelper.TABLE_TEACHERS + " WHERE " + DBHelper.PREP_TEACHERS + "='" +input+"'";
            Cursor emp=sqdb.rawQuery(empty, null);
            emp.moveToFirst();
            return emp.getString(emp.getColumnIndex(DBHelper.FIO_TEACHERS));
        }
		else
			return "";
	}
    public String[] readFavoriteInput(int type){
        ArrayList<String> data = new ArrayList<String>();
        String[] output = null;
        String query = "SELECT "+DBHelper.SAVED_INPUT+" FROM " + DBHelper.TABLE_SAVED + " WHERE " + DBHelper.SAVED_TYPE + "='" + type + "' GROUP BY " + DBHelper.SAVED_INPUT;
        Cursor cursor=sqdb.rawQuery(query, null);
        while (cursor.moveToNext()) {
            data.add(cursor.getString(cursor.getColumnIndex(DBHelper.SAVED_INPUT)));
        }
        output = new String[data.size()];
        data.toArray(output);
        return output;
    }
    public String[] readFavorite(){

        ArrayList<String> data = new ArrayList<String>();
        String query = "SELECT * FROM " + DBHelper.TABLE_SAVED;
        Cursor cursor = sqdb.rawQuery(query, null);
        while (cursor.moveToNext()){
            data.add(cursor.getString(cursor.getColumnIndex(DBHelper.SAVED_INPUT))+"_"+
                    cursor.getString(cursor.getColumnIndex(DBHelper.SAVED_TYPE)));
        }
        return data.toArray(new String[data.size()]);
    }


	public String[] readDate(String vid,String input){
		ArrayList<String> dates = new ArrayList<String>();
		String[] output = null;
		String empty = null;
		if (vid.equals("2") || vid.equals("4"))
			empty = "SELECT "+ DBHelper.DZ_DMAIN +" FROM "+DBHelper.TABLE_DMAIN + " WHERE " + DBHelper.GRUP_DMAIN + "='" +input+"' GROUP BY " + DBHelper.DZ_DMAIN ;
		else if (vid.equals("3"))
			empty = "SELECT "+ DBHelper.DZ_DMAIN +" FROM "+DBHelper.TABLE_DMAIN + " WHERE " + DBHelper.PREP_DMAIN + "='" +input+"' GROUP BY " + DBHelper.DZ_DMAIN ;
		else if (vid.equals("5"))
			empty = "SELECT "+ DBHelper.DZ_DMAIN +" FROM "+DBHelper.TABLE_DMAIN + " WHERE " + DBHelper.AUD_DMAIN + "='" +input+"' GROUP BY " + DBHelper.DZ_DMAIN ;
        else if (vid.equals("6"))
            empty = "SELECT "+ DBHelper.DZ_DMAIN +" FROM "+DBHelper.TABLE_DMAIN + " WHERE " + DBHelper.TIP_DMAIN +"='s' AND " + DBHelper.GRUP_DMAIN + "='" +input+"' GROUP BY " + DBHelper.DZ_DMAIN ;
        Log.i("DZ", empty);
		Cursor emp=sqdb.rawQuery(empty, null);
		while (emp.moveToNext()) {
			dates.add(emp.getString(emp.getColumnIndex(DBHelper.DZ_DMAIN)));

		}
		output = new String[dates.size()];
		dates.toArray(output);
		return output;
	}	
	
	//ListForHints
	public ArrayList<String> readListForHints(int i){
		ArrayList<String> input = new ArrayList<String>();
		String sql;
		Cursor c;
		if (i==0){
			sql="SELECT " + DBHelper.GROUP_NUMBER_MAIN + " FROM " +DBHelper.TABLE_MAIN + " GROUP BY " + DBHelper.GROUP_NUMBER_MAIN ;
			c = sqdb.rawQuery(sql,null);
			while (c.moveToNext()) {
				if (c.getString(c.getColumnIndex(DBHelper.GROUP_NUMBER_MAIN))!=""){
					input.add(c.getString(c.getColumnIndex(DBHelper.GROUP_NUMBER_MAIN)));	
					
				}
			}
		}  else if (i==1 || i==3){
			sql="SELECT " + DBHelper.FIO_TEACHERS + " FROM " +DBHelper.TABLE_TEACHERS;
			c=sqdb.rawQuery(sql, null);
			while (c.moveToNext()) {
					input.add(c.getString(c.getColumnIndex(DBHelper.FIO_TEACHERS)));
			}
		}
		else if (i==2){
			sql = "SELECT " + DBHelper.GRUP_DMAIN + " FROM " + DBHelper.TABLE_DMAIN + " WHERE " + DBHelper.TIP_DMAIN + "='z' GROUP BY " + DBHelper.GRUP_DMAIN;
			c=sqdb.rawQuery(sql, null);
			while (c.moveToNext()) {
				if(c.getString(c.getColumnIndex(DBHelper.GRUP_DMAIN)).length()<7)
					input.add(c.getString(c.getColumnIndex(DBHelper.GRUP_DMAIN)));
			}
		}
		else if (i==4){
			sql = "SELECT " + DBHelper.GRUP_DMAIN + " FROM " + DBHelper.TABLE_DMAIN + " WHERE " + DBHelper.TIP_DMAIN + "='c' GROUP BY " + DBHelper.GRUP_DMAIN;
			c=sqdb.rawQuery(sql, null);
			while (c.moveToNext()) {
					input.add(c.getString(c.getColumnIndex(DBHelper.GRUP_DMAIN)));
			}
		}
		else if (i==5){
			sql = "SELECT " + DBHelper.AUD_AUDITORY + " FROM " + DBHelper.TABLE_AUDITORY;
			c=sqdb.rawQuery(sql, null);
			while (c.moveToNext()) {
					input.add(c.getString(c.getColumnIndex(DBHelper.AUD_AUDITORY)));
			}
		}
        else if (i==6){
            sql = "SELECT " + DBHelper.GRUP_DMAIN + " FROM " + DBHelper.TABLE_DMAIN + " WHERE " + DBHelper.TIP_DMAIN + "='s' GROUP BY " + DBHelper.GRUP_DMAIN;
            c=sqdb.rawQuery(sql, null);
            while (c.moveToNext()) {
                input.add(c.getString(c.getColumnIndex(DBHelper.GRUP_DMAIN)));
            }
        }
		return input;
	}

	
	String convertTime(String time){
		String time1="";
		char pair_number1=time.charAt(0);
		if (pair_number1=='1')
			time1="08:00";
		else if (pair_number1=='2')
			time1="09:40";
		else if (pair_number1=='3')
			time1="11:20";
		else if (pair_number1=='4')
			time1="13:00";
		else if (pair_number1=='5')
			time1="14:40";
		else if (pair_number1=='6')
			time1="16:20";
		else if (pair_number1=='7')
			time1="18:00";
		else if (pair_number1=='8')
			time1="19:40";
		else
			time1="12:00";
		return time1;
	}
	///////////
	public String lastLesson(String vid,String input,String day,String week){
		
		 String sql = null;
		 Cursor c;
		 if (vid.equals("0") || vid.equals("1")){
			 sql = "SELECT " + DBHelper.PAIR_NUMBER_MAIN + " FROM " + DBHelper.TABLE_MAIN + " WHERE "  +
				 DBHelper.GROUP_NUMBER_MAIN + " ='" + input + "' AND " + DBHelper.DAY_OF_WEEK_MAIN + " ='" + day + "' AND "+ DBHelper.WEEK_MAIN + " ='" + week + "'"  + " ORDER BY " + DBHelper.PAIR_NUMBER_MAIN;
		 }
		 else if (vid.equals("3")){
			 sql = "SELECT " + DBHelper.PARA_DMAIN + " FROM " + DBHelper.TABLE_DMAIN 
					 + " WHERE " + DBHelper.PREP_DMAIN +" ='" + input+"'" +" AND " + DBHelper.DZ_DMAIN  + "='"+ day+"' ORDER BY " + DBHelper.PARA_DMAIN;
			
		 }
		 else if (vid.equals("2") || vid.equals("4")){
			 sql = "SELECT " + DBHelper.PARA_DMAIN + " FROM " + DBHelper.TABLE_DMAIN 
					 + " WHERE " + DBHelper.GRUP_DMAIN +" ='" + input+"'" +" AND " + DBHelper.DZ_DMAIN  + "='"+ day+"' ORDER BY " + DBHelper.PARA_DMAIN;
		 }
		 else if (vid.equals("5")){
			 sql = "SELECT " + DBHelper.PARA_DMAIN + " FROM " + DBHelper.TABLE_DMAIN 
					 + " WHERE " + DBHelper.AUD_DMAIN +" ='" + input+"'" +" AND " + DBHelper.DZ_DMAIN  + "='"+ day+"' ORDER BY " + DBHelper.PARA_DMAIN;
		 }
		 c = sqdb.rawQuery(sql, null);
		 c.moveToLast();
		 String time="";
		 //char pair_number=c.getString(c.getColumnIndex(DBHelper.PAIR_NUMBER_MAIN)).charAt(0);
		 time = convertTime(c.getString(c.getColumnIndex(DBHelper.PAIR_NUMBER_MAIN)));
		 return(time);
		 
	 }
	///////////
	public ArrayList<MainTimetable> readMain(String k,String day,String week,String input){
		if (k.equals("0")){
			String sqlquery = "SELECT " + DBHelper.PAIR_NUMBER_MAIN + "," + DBHelper.AUDITORY_MAIN+ ","+DBHelper.DISS_SUBJECTS+","+DBHelper.DISP_SUBJECTS + ","+DBHelper.PAIR_TYPE_MAIN + ","+ DBHelper.FIO_TEACHERS
					+ " FROM " + DBHelper.TABLE_MAIN + " JOIN " + DBHelper.TABLE_SUBJECTS + " ON " + DBHelper.TABLE_MAIN + "." + DBHelper.SUBJECT_ID_MAIN 
					+ "=" + DBHelper.TABLE_SUBJECTS + "." +DBHelper.DIS_SUBJECTS + " JOIN "+ DBHelper.TABLE_TEACHERS + " ON " + DBHelper.TABLE_MAIN + "." + DBHelper.TEACHER_ID_MAIN + "="+ DBHelper.TABLE_TEACHERS +"." +DBHelper.PREP_TEACHERS
					+ " WHERE " + DBHelper.GROUP_NUMBER_MAIN + " ='" + input + "' AND " + DBHelper.DAY_OF_WEEK_MAIN + " ='" + day + "' AND "+ DBHelper.WEEK_MAIN + " ='" + week + "'"  + " ORDER BY " + DBHelper.PAIR_NUMBER_MAIN ;
					Cursor c = sqdb.rawQuery(sqlquery,null);
					ArrayList<MainTimetable> lessons = new ArrayList<MainTimetable>();
					while (c.moveToNext()) {

						String time="";
						char pair_number=c.getString(c.getColumnIndex(DBHelper.PAIR_NUMBER_MAIN)).charAt(0);
						time = getTime(time, pair_number);

						MainTimetable timetable = new MainTimetable();
						timetable.setTimetable(time, c.getString(c.getColumnIndex(DBHelper.AUDITORY_MAIN)),
								c.getString(c.getColumnIndex(DBHelper.FIO_TEACHERS)), c.getString(c.getColumnIndex(DBHelper.PAIR_TYPE_MAIN)),
								c.getString(c.getColumnIndex(DBHelper.DISS_SUBJECTS)));
						lessons.add(timetable);
					}
					return lessons;
		}
		else if (k.equals("1")){
			ArrayList<MainTimetable> preps = new ArrayList<MainTimetable>();
			ArrayList<String> grups = new ArrayList<String>();
			ArrayList<String> times = new ArrayList<String>();
			String sqlquery1 = "SELECT " + DBHelper.PAIR_NUMBER_MAIN + ","+DBHelper.AUDITORY_MAIN+ "," + DBHelper.DISS_SUBJECTS +","+DBHelper.DISP_SUBJECTS + ","+DBHelper.PAIR_TYPE_MAIN + ","+ DBHelper.GROUP_NUMBER_MAIN
					+ " FROM " + DBHelper.TABLE_MAIN + " JOIN " + DBHelper.TABLE_SUBJECTS + " ON " + DBHelper.TABLE_MAIN + "." + DBHelper.SUBJECT_ID_MAIN 
					+ "=" + DBHelper.TABLE_SUBJECTS + "." +DBHelper.DIS_SUBJECTS + " JOIN "+ DBHelper.TABLE_TEACHERS + " ON " + DBHelper.TABLE_MAIN + "." + DBHelper.TEACHER_ID_MAIN + "="+ DBHelper.TABLE_TEACHERS +"." +DBHelper.PREP_TEACHERS
					+ " WHERE " + DBHelper.TEACHER_ID_MAIN + " ='"+input+ "' AND " + DBHelper.DAY_OF_WEEK_MAIN + " ='" + day + "' AND "+ DBHelper.WEEK_MAIN + " ='" + week + "'"  + " ORDER BY " + DBHelper.PAIR_NUMBER_MAIN ;
			Cursor c1 = sqdb.rawQuery(sqlquery1,null);
			while (c1.moveToNext()) {
				String time1="";
				char pair_number1=c1.getString(c1.getColumnIndex(DBHelper.PAIR_NUMBER_MAIN)).charAt(0);
				time1 = getTime(time1, pair_number1);
				MainTimetable timetable  = new MainTimetable();
				timetable.setTimetable(time1, c1.getString(c1.getColumnIndex(DBHelper.AUDITORY_MAIN)),
						c1.getString(c1.getColumnIndex(DBHelper.GROUP_NUMBER_MAIN)),c1.getString(c1.getColumnIndex(DBHelper.PAIR_TYPE_MAIN)),
						c1.getString(c1.getColumnIndex(DBHelper.DISS_SUBJECTS)));

				preps.add(timetable);

				times.add(time1);
				grups.add(c1.getString(c1.getColumnIndex(DBHelper.GROUP_NUMBER_MAIN)));
			}
			for (int i=0;i<times.size()-1;i++)
			{
				if (times.get(i).equals(times.get(i+1)))
				{
					preps.get(i).setFirstType(grups.get(i) + "," + grups.get(i + 1));
					grups.set(i, grups.get(i) + "," + grups.get(i + 1));
					preps.remove(i+1);
					times.remove(i+1);
					grups.remove(i+1);
					i=-1;
				}
			}
			return preps;
		}
		return null;	
	}

	public ArrayList<String> getAllDays(int type,String value){
		ArrayList<String> result = new ArrayList<String>();
		String sqlQuery;
		if (type==3){
			sqlQuery = "SELECT " + DBHelper.DZ_DMAIN + " FROM " + DBHelper.TABLE_DMAIN + " WHERE " +
					DBHelper.PREP_DMAIN + " ='" + value +"' GROUP BY " + DBHelper.DZ_DMAIN;
			Cursor cursor = sqdb.rawQuery(sqlQuery,null);
			while (cursor.moveToNext()) {
				result.add(cursor.getString(cursor.getColumnIndex(DBHelper.DZ_DMAIN)));
			}
		}
		else if (type==5){
			sqlQuery = "SELECT " + DBHelper.DZ_DMAIN + " FROM " + DBHelper.TABLE_DMAIN + " WHERE " +
					DBHelper.AUD_DMAIN + " ='" + value +"' GROUP BY " + DBHelper.DZ_DMAIN;
			Cursor cursor = sqdb.rawQuery(sqlQuery,null);
			while (cursor.moveToNext()) {
				result.add(cursor.getString(cursor.getColumnIndex(DBHelper.DZ_DMAIN)));
			}
		}
		else{
			sqlQuery = "SELECT " + DBHelper.DZ_DMAIN + " FROM " + DBHelper.TABLE_DMAIN + " WHERE " +
					DBHelper.GRUP_DMAIN + " ='" + value +"' GROUP BY " + DBHelper.DZ_DMAIN;
			Cursor cursor = sqdb.rawQuery(sqlQuery,null);
			while (cursor.moveToNext()) {
				result.add(cursor.getString(cursor.getColumnIndex(DBHelper.DZ_DMAIN)));
			}
		}
		return result;
	}

	private String getTime(String time, char pair_number) {
		if (pair_number=='1')
            time="08:00";
        else if (pair_number=='2')
            time="09:40";
        else if (pair_number=='3')
            time="11:20";
        else if (pair_number=='4')
            time="13:00";
        else if (pair_number=='5')
            time="14:40";
        else if (pair_number=='6')
            time="16:20";
        else if (pair_number=='7')
            time="18:00";
        else if (pair_number=='8')
            time="19:40";
		return time;
	}

	//////////
	public ArrayList<MainTimetable> readDmain(String k,String input,String date){
		ArrayList<MainTimetable> lessons = new ArrayList<MainTimetable>();
		if (k.equals("2") || k.equals("4")){
				String sqlquery1 = "SELECT " +DBHelper.PARA_DMAIN+","+DBHelper.DISS_SUBJECTS+","+DBHelper.VID_DMAIN+","+DBHelper.AUD_DMAIN+ ","+DBHelper.FIO_TEACHERS+","+DBHelper.DISP_SUBJECTS+ " FROM " + DBHelper.TABLE_DMAIN + " JOIN " + DBHelper.TABLE_SUBJECTS + " ON " + DBHelper.TABLE_DMAIN + "." + DBHelper.DIS_DMAIN
						+ "=" + DBHelper.TABLE_SUBJECTS + "." +DBHelper.DIS_SUBJECTS + " JOIN "+ DBHelper.TABLE_TEACHERS + " ON " + DBHelper.TABLE_DMAIN + "." + DBHelper.PREP_DMAIN + "="+ DBHelper.TABLE_TEACHERS +"." +DBHelper.PREP_TEACHERS
						+ " WHERE " + DBHelper.GRUP_DMAIN +" ='" + input+"'" +" AND " + DBHelper.DZ_DMAIN  + "='"+ date+"' ORDER BY " + DBHelper.PARA_DMAIN;
				Cursor c1 = sqdb.rawQuery(sqlquery1,null);
			while (c1.moveToNext()) {
				MainTimetable timetable = new MainTimetable();

				String time = convertTime(c1.getString(c1.getColumnIndex(DBHelper.PARA_DMAIN)));
					timetable.setTimetable(time, c1.getString(c1.getColumnIndex(DBHelper.AUD_DMAIN)),
							c1.getString(c1.getColumnIndex(DBHelper.FIO_TEACHERS)), c1.getString(c1.getColumnIndex(DBHelper.VID_DMAIN)),
							c1.getString(c1.getColumnIndex(DBHelper.DISS_SUBJECTS)
							));
					Log.e("TIMETABLE",timetable.toString());
					lessons.add(timetable);
				}
				return lessons;
			}
		 else if (k.equals("3")){
				String sqlquery1 = "SELECT " +DBHelper.PARA_DMAIN+","+DBHelper.DISS_SUBJECTS+","+DBHelper.VID_DMAIN+","+DBHelper.AUD_DMAIN+ ","+DBHelper.GRUP_DMAIN+","+DBHelper.DISP_SUBJECTS+ " FROM " + DBHelper.TABLE_DMAIN + " JOIN " + DBHelper.TABLE_SUBJECTS + " ON " + DBHelper.TABLE_DMAIN + "." + DBHelper.DIS_DMAIN
						+ "=" + DBHelper.TABLE_SUBJECTS + "." +DBHelper.DIS_SUBJECTS 
						+ " WHERE " + DBHelper.PREP_DMAIN +" ='" + input+"'" +" AND " + DBHelper.DZ_DMAIN  + "='"+ date+"' ORDER BY " + DBHelper.PARA_DMAIN;
				Cursor c1 = sqdb.rawQuery(sqlquery1,null);
				while (c1.moveToNext()) {
					MainTimetable timetable = new MainTimetable();

					String time = convertTime(c1.getString(c1.getColumnIndex(DBHelper.PARA_DMAIN)));
					timetable.setTimetable(time, c1.getString(c1.getColumnIndex(DBHelper.AUD_DMAIN)),
							c1.getString(c1.getColumnIndex(DBHelper.GRUP_DMAIN)), c1.getString(c1.getColumnIndex(DBHelper.VID_DMAIN)),
							c1.getString(c1.getColumnIndex(DBHelper.DISS_SUBJECTS)
							));
					lessons.add(timetable);
				}
				return lessons;
		 }
		 else if (k.equals("5")){
				String sqlquery1 = "SELECT " +DBHelper.PARA_DMAIN+","+DBHelper.DISS_SUBJECTS+","+DBHelper.VID_DMAIN+","+DBHelper.PREP_DMAIN+ ","+DBHelper.GRUP_DMAIN+","+DBHelper.DISP_SUBJECTS+ " FROM " + DBHelper.TABLE_DMAIN +" JOIN " +
						DBHelper.TABLE_SUBJECTS + " ON " + DBHelper.TABLE_DMAIN + "." + DBHelper.DIS_DMAIN + "=" + DBHelper.TABLE_SUBJECTS+"."+DBHelper.DIS_SUBJECTS
						+ " WHERE " + DBHelper.AUD_DMAIN +" ='" + input+"'" +" AND " + DBHelper.DZ_DMAIN  + "='"+ date+"' ORDER BY " + DBHelper.PARA_DMAIN;
				Cursor c1 = sqdb.rawQuery(sqlquery1,null);
				while (c1.moveToNext()) {
					MainTimetable timetable = new MainTimetable();

					String time = convertTime(c1.getString(c1.getColumnIndex(DBHelper.PARA_DMAIN)));
					timetable.setTimetable(time, c1.getString(c1.getColumnIndex(DBHelper.PREP_DMAIN)),
							c1.getString(c1.getColumnIndex(DBHelper.GRUP_DMAIN)), c1.getString(c1.getColumnIndex(DBHelper.VID_DMAIN)),
							c1.getString(c1.getColumnIndex(DBHelper.DISS_SUBJECTS)
							));
					lessons.add(timetable);
				}
				return lessons;
		 }
         else if (k.equals("6")){
             String sqlquery1 = "SELECT " +DBHelper.PARA_DMAIN+","+DBHelper.DISS_SUBJECTS+","+DBHelper.VID_DMAIN+","+DBHelper.AUD_DMAIN+ ","+DBHelper.FIO_TEACHERS+","+DBHelper.DISP_SUBJECTS+ " FROM " + DBHelper.TABLE_DMAIN + " JOIN " + DBHelper.TABLE_SUBJECTS + " ON " + DBHelper.TABLE_DMAIN + "." + DBHelper.DIS_DMAIN
                     + "=" + DBHelper.TABLE_SUBJECTS + "." +DBHelper.DIS_SUBJECTS + " JOIN "+ DBHelper.TABLE_TEACHERS + " ON " + DBHelper.TABLE_DMAIN + "." + DBHelper.PREP_DMAIN + "="+ DBHelper.TABLE_TEACHERS +"." +DBHelper.PREP_TEACHERS
                     + " WHERE " + DBHelper.VID_DMAIN+"='ЭК' AND "+ DBHelper.GRUP_DMAIN +" ='" + input+"'" +" AND " + DBHelper.DZ_DMAIN  + "='"+ date+"' ORDER BY " + DBHelper.PARA_DMAIN;
             Cursor c1 = sqdb.rawQuery(sqlquery1,null);
             while (c1.moveToNext()) {
				 MainTimetable timetable = new MainTimetable();

				 String time = convertTime(c1.getString(c1.getColumnIndex(DBHelper.PARA_DMAIN)));
				 timetable.setTimetable(time, c1.getString(c1.getColumnIndex(DBHelper.AUD_DMAIN)),
						 c1.getString(c1.getColumnIndex(DBHelper.GRUP_DMAIN)), c1.getString(c1.getColumnIndex(DBHelper.VID_DMAIN)),
						 c1.getString(c1.getColumnIndex(DBHelper.DISS_SUBJECTS)
						 ));
				 lessons.add(timetable);
             }
             return lessons;
         }
		 return null;
		
	}


}
