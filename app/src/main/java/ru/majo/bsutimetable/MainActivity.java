package ru.majo.bsutimetable;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import me.drakeet.materialdialog.MaterialDialog;

public class MainActivity extends AppCompatActivity
        implements TimetableDaysAdapter.Listener,TimetableViewAdapter.TimetableClickListener,
        TimetableSelectAdapter.Listener,TimetableFavoriteAdapter.Listener,ChoiceFragment.CallbackListener {

    //private String prefix ="http://www.bsu.ru";
    private String prefix ="http://192.168.56.1:8080";
    private String url1 = prefix + "/content/rasp/rasp_dmain.json";
    private String url2 = prefix + "/content/rasp/rasp_group.json";
    private String url3 = prefix + "/content/rasp/rasp_auditory.json";
    private String url4 = prefix + "/content/rasp/rasp_main.json";
    private String url5 = prefix + "/content/rasp/rasp_subjects.json";
    private String url6 = prefix + "/content/rasp/rasp_teachers.json";

    private Toolbar mToolbar;
    private TextView mToolbarTitle;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private NavigationView mNavigationView;
    private View mHeaderView;

    private DBReader mDBReader;
    private SharedPreferences mSP;
    private boolean mIsEmpty = true;

    private MaterialDialog mMaterialDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSP = getSharedPreferences("settings", Context.MODE_PRIVATE);
        mDBReader = new DBReader(this);

        ifFirstTime();

        if (!mSP.getString("value_save","null").equals("null"))
            mIsEmpty = false;

        setUpToolbar();
        setUpNavDrawer();
        setFirstFragment();
        isDatabaseCorrputed();
        isTodayToUpdate();
    }


    private void ifFirstTime(){
        if (mSP.getBoolean("this_is_third_time", true)) {
            File file = new File("/data/data/ru.majo.bsutimetable/databases/stud.db");
            Log.i("My Logs","NOT VERY FIRST");
            file.delete();
            try {
                Log.i("My Logs","NOT VERY FIRST");
                file.getCanonicalFile().delete();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            Log.i("My Logs","MY FIRST");
            mSP.edit().putBoolean("this_is_third_time", false).commit();
            writeReserveDatabase();
        }
        Log.i("My Logs", "IS FIRST?");

        //File file = new File("/data/data/ru.majo.bsutimetable/databases/stud.db");
        mDBReader = new DBReader(this);

    }

    private void setUpToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
            mToolbarTitle = (TextView)findViewById(R.id.toolbar_text);
        }
    }

    private void setUpNavDrawer() {
        if (mToolbar != null) {
            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            mNavigationView = (NavigationView) findViewById(R.id.navigation_view);

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
            });
            mDrawerToggle = new ActionBarDrawerToggle(this,mDrawerLayout,mToolbar,R.string.app_name,R.string.app_name){
                @Override
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                }
                @Override
                public void onDrawerClosed(View drawerView) {
                    super.onDrawerClosed(drawerView);
                }
            };
            mDrawerLayout.setDrawerListener(mDrawerToggle);
            mDrawerToggle.syncState();

            mHeaderView = getLayoutInflater().inflate(R.layout.drawer_header,null);

            mNavigationView.addHeaderView(mHeaderView);
            if (!mIsEmpty)
                setNavigationHeader();

            mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(MenuItem menuItem) {
                    menuItem.setChecked(true);
                    mDrawerLayout.closeDrawers();
                    clearBackstack();
                    switch (menuItem.getItemId()) {
                        case R.id.drawer_home:
                            setFirstFragment();
                            return true;
                        case R.id.drawer_timetable:
                            mToolbarTitle.setText("Расписание");
                            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                                    new TimetableViewFragment()).commit();
                            return true;
                        case R.id.drawer_settings:
                            mToolbarTitle.setText("Настройки");
                            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                                    new SettingsFragment()).commit();
                            return true;
                        case R.id.drawer_favorites:
                            mToolbarTitle.setText("Избранное");
                            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                                    new TimetableFavoriteFragment()).commit();
                            return true;
                        default:
                            return true;
                    }
                }
            });
        }
    }

    private void setFirstFragment() {
        if (mIsEmpty)
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    ChoiceFragment.newInstance()).commit();
        else {
            String value = mSP.getString("value_save", new String());
            if (mSP.getInt("type_save", 0)==1 || mSP.getInt("type_save", 0)==3)
                value = mDBReader.convertbackTeacher(value);
            mToolbarTitle.setText(value);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    TimetableTabsFragment.newInstance(mSP.getInt("type_save", -1),
                            mSP.getString("value_save", new String()))).commit();
        }
    }

    public Toolbar getToolbar(){
        return mToolbar;
    }

    public TextView getToolbarTitle(){
        return mToolbarTitle;
    }

    private void writeReserveDatabase() {
        try {
            Log.i("My Logs", "FIRTVERY");
            AssetManager assetManager = getResources().getAssets();
            InputStream myInput = assetManager.open("stud.db");
            String outFileName = "data/data/ru.majo.bsutimetable/databases/stud.db";
            OutputStream myOutput = new FileOutputStream(outFileName);
            byte[] buffer = new byte[8192];
            int length;
            while ((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
                Log.i("SD",Integer.toString(length));
            }
            myOutput.flush();
            myOutput.close();
            myInput.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void isDatabaseCorrputed(){
        if (Utils.isDatabaseCorrupted(this)){
            Log.e("CORRUPTED","isDatabaseCorrupted");
            writeReserveDatabase();
        }
    }

    private void isTodayToUpdate(){
        if (mSP.getBoolean("automatic_update",false)) {
            String updateDate = mSP.getString("automatic_update_date", "2015-11-04");
            if (Utils.isWeekDifference(updateDate)) {
                setDialog();
            }
        }
    }

    private void setDialog(){
        mMaterialDialog = new MaterialDialog(this)
                .setTitle("Обновление расписания")
                .setMessage("Обновить базу расписания?(Может потребоваться несколько минут)")
                .setPositiveButton("Ок", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startUpdate();
                        mMaterialDialog.dismiss();
                    }
                })
                .setNegativeButton("Отменить", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();
                    }
                });

        mMaterialDialog.show();
    }

    private void startUpdate(){
        if (Utils.checkInternet(this)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startReadJson();
                }
            },10);
        }
        else
            Toast.makeText(this, "Отсутствует подключение к интернету", Toast.LENGTH_SHORT).show();
    }

    private void startReadJson() {
        new SettingsFragment.JsonReadTask(this)
                .execute(new String[]{url1, url2, url3, url4, url5, url6});
    }

    @Override
    public void sendType(int type) {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, TimetableSelectFragment.newInstance(type)
        ).addToBackStack("TimetableSelectFragment").commit();
    }


    @Override
    public void getDayMain(int type, String value, int week, int day) {
        mToolbarTitle.setText(value);
        if (type==1)
            mToolbarTitle.setText(mDBReader.convertbackTeacher(value));
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                TimetableLessonsFragment.newInstance(type,value,week,day)).addToBackStack("lesson").commit();
    }

    @Override
    public void getDayDMain(int type, String value, String date) {
        mToolbarTitle.setText(value);
        if (type==3)
            mToolbarTitle.setText(mDBReader.convertbackTeacher(value));
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                TimetableLessonsFragment.newInstance(type,value,date)).addToBackStack("lesson").commit();
    }

    @Override
    public void getValue(int type, String value) {
        mToolbarTitle.setText(value);
        if (type==1 || type==3)
            value = mDBReader.convertTeacher(value);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                TimetableTabsFragment.newInstance(type, value)).addToBackStack("TimetableTabsFragment").commit();

    }

    @Override
    public void onChange() {
        mNavigationView.getMenu().getItem(0).setChecked(true);
        setNavigationHeader();
    }

    private void setNavigationHeader() {

        mIsEmpty = false;

        TextView textType = (TextView)mHeaderView.findViewById(R.id.header_text_type);
        TextView textHeader = (TextView)mHeaderView.findViewById(R.id.header_text);


        int type = mSP.getInt("type_save", 0);
        String value = mSP.getString("value_save", new String());
        textType.setText(Utils.getType(type));
        if (type==1 || type==3)
            value = mDBReader.convertbackTeacher(value);
        textHeader.setText(value);
    }

    private void clearBackstack(){
        FragmentManager fm = getSupportFragmentManager();
        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }
}
