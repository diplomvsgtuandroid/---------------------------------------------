package ru.majo.bsutimetable;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.lang.ref.WeakReference;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Majo on 02.07.2015.
 */
public class TimetableTabsFragment extends Fragment {

    private DBReader mDBReader;
    TabLayout tabLayout;
    ViewPager viewPager;
    FrameLayout container;
    ArrayList<Fragment> mFragmentsMainTimetable;

    private int mType;
    private String mValue;

    private String mFirstWeek;
    private String mSecondWeek;

    public static TimetableTabsFragment newInstance(int type,String value){
        TimetableTabsFragment timetableTabsFragment = new TimetableTabsFragment();
        Bundle bundle = new Bundle(2);
        bundle.putInt("type", type);
        bundle.putString("value", value);
        timetableTabsFragment.setArguments(bundle);
        return timetableTabsFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_timetable_weeks,null);
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        container = (FrameLayout)view.findViewById(R.id.child_container);
        tabLayout = (TabLayout) view.findViewById(R.id.sliding_tabs);

        mDBReader = new DBReader(getActivity());
        mType = getArguments().getInt("type");
        mValue = getArguments().getString("value");
        setTitle(mValue,mType);
        if (mType<2) {
            tabLayout.setTabMode(TabLayout.MODE_FIXED);
            viewPager.setVisibility(View.GONE);
            container.setVisibility(View.VISIBLE);
            setupTabs(mValue);
        } else {
            setupViewPager(viewPager, mType, mValue);
            tabLayout.setupWithViewPager(viewPager);
        }
        return view;
    }

    private void setTitle(String value,int type){
        String valueText = value;
        if (type==1 || type==3)
            valueText = mDBReader.convertbackTeacher(value);
        ((MainActivity)getActivity()).getToolbarTitle().setText(valueText);
    }

    private void setupTabs(String value) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        mFragmentsMainTimetable = new ArrayList<>();
        mFragmentsMainTimetable.add(TimetableMainDaysFragment.newInstance(mType, value, 1));
        mFragmentsMainTimetable.add(TimetableMainDaysFragment.newInstance(mType, value, 2));

        TabLayout.Tab firstTab = tabLayout.newTab();
        TabLayout.Tab secondTab = tabLayout.newTab();

        tabLayout.addTab(firstTab);
        tabLayout.addTab(secondTab);

        if (Utils.getParity(dateFormat.format(new Date()))==1){
            mFirstWeek = "1 неделя (Т)";
            mSecondWeek = "2 неделя";
            firstTab.select();
            getChildFragmentManager().beginTransaction().
                    replace(R.id.child_container, mFragmentsMainTimetable.get(0)).commit();
        }else{
            mFirstWeek = "1 неделя";
            mSecondWeek = "2 неделя (Т)";
            secondTab.select();
            getChildFragmentManager().beginTransaction().
                    replace(R.id.child_container,mFragmentsMainTimetable.get(1)).commit();
        }
        firstTab.setText(mFirstWeek);
        secondTab.setText(mSecondWeek);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getText().equals(mFirstWeek))
                    getChildFragmentManager().beginTransaction().
                            replace(R.id.child_container, mFragmentsMainTimetable.get(0)).commit();
                else
                    getChildFragmentManager().beginTransaction().
                            replace(R.id.child_container,mFragmentsMainTimetable.get(1)).commit();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setupViewPager(final ViewPager viewPager,int type, String value) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        ArrayList<String> months = new ArrayList<String>();
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        if (type<2) {
            if (Utils.getParity(dateFormat.format(new Date()))==1){
                adapter.addFrag(TimetableMainDaysFragment.newInstance(mType, value, 1), "1 неделя (Т)");
                adapter.addFrag(TimetableMainDaysFragment.newInstance(mType, value, 2), "2 неделя");
            }else{
                adapter.addFrag(TimetableMainDaysFragment.newInstance(mType, value, 1), "1 неделя");
                adapter.addFrag(TimetableMainDaysFragment.newInstance(mType, value, 2), "2 неделя (Т)");
            }
        }
        else{
            ArrayList<String> allDays = mDBReader.getAllDays(mType,value);
            for (int i=1;i<13;i++){
                if(Utils.isHaveMonth(i, allDays)) {
                    months.add(Utils.getMonth(i));
                    if (Utils.getMonth(getCurrentMonth()+1).equals(Utils.getMonth(i)))
                        adapter.addFrag(TimetableDaysFragment.
                                newInstance(mType, value, getDays(i, allDays),true), Utils.getMonth(i));
                    else
                        adapter.addFrag(TimetableDaysFragment.
                                newInstance(mType, value, getDays(i, allDays),false), Utils.getMonth(i));

                }
            }
        }
        viewPager.setAdapter(adapter);
        if (mType<2) {
            viewPager.setCurrentItem(Utils.getParity(dateFormat.format(new Date())) - 1);
            viewPager.setEnabled(false);
        }
        else{
            int position = 0;
            for (int i=0;i<months.size();i++){
                if (months.get(i).equals(Utils.getMonth(getCurrentMonth() + 1)))
                    position=i;
            }
            viewPager.setCurrentItem(position);
        }

    }
    private int getCurrentMonth(){
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        return cal.get(Calendar.MONTH);
    }

    private ArrayList<String> getDays(int month, ArrayList<String> days) {
        DateFormat dateFormat  = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        ArrayList<String> result = new ArrayList<String>();
        for (int i=0;i<days.size();i++){
            try {
                cal.setTime(dateFormat.parse(days.get(i)));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (month==(cal.get(Calendar.MONTH)+1))
                result.add(days.get(i));
        }
        return result;
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }
        @Override
        public int getCount() {
            return mFragmentList.size();
        }
        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
