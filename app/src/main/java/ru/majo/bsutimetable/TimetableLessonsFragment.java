package ru.majo.bsutimetable;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * Created by Majo on 04.07.2015.
 */
public class TimetableLessonsFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private AppBarLayout mHeaderLayout;
    private DBReader mDbReader;

    private TextView mEmptyListTextView;
    private TextView mHeaderFirstTextView;
    private TextView mHeaderSecondTextView;

    private View mShadowView;

    public static TimetableLessonsFragment newInstance(int type,String value,int week, int day){
        TimetableLessonsFragment timetableLessonsFragment = new TimetableLessonsFragment();
        Bundle bundle = new Bundle(4);
        bundle.putInt("type", type);
        bundle.putString("value", value);
        bundle.putInt("week", week);
        bundle.putInt("day", day);
        timetableLessonsFragment.setArguments(bundle);
        return timetableLessonsFragment;
    }

    public static TimetableLessonsFragment newInstance(int type,String value,String date){
        TimetableLessonsFragment timetableLessonsFragment = new TimetableLessonsFragment();
        Bundle bundle = new Bundle(3);
        bundle.putInt("type", type);
        bundle.putString("value",value);
        bundle.putString("date", date);
        timetableLessonsFragment.setArguments(bundle);
        return timetableLessonsFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_timetable_lessons,null);

        mShadowView = (View)view.findViewById(R.id.shadow_view);

        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
            mShadowView.setVisibility(View.VISIBLE);

        mRecyclerView = (RecyclerView)view.findViewById(R.id.lessons_recyclerview);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mHeaderLayout = (AppBarLayout)view.findViewById(R.id.layout_header_appbar);
        mHeaderFirstTextView = (TextView)view.findViewById(R.id.lesson_week_textview);
        mHeaderSecondTextView = (TextView)view.findViewById(R.id.lesson_day_textview);
        mEmptyListTextView = (TextView)view.findViewById(R.id.lessons_textview);

        mHeaderLayout.setTranslationY(-250);
        mDbReader = new DBReader(getActivity());

        animateHeader();

        ArrayList<MainTimetable> lessons;
        if (getArguments().getInt("type")<2) {
            Log.e("LESSONS", String.valueOf(getArguments().getInt("day")) + " " + String.valueOf(getArguments().getInt("week")));
            lessons = mDbReader.readMain(String.valueOf(getArguments().getInt("type")), String.valueOf(getArguments().getInt("day")),
                    String.valueOf(getArguments().getInt("week")), getArguments().getString("value"));
            mHeaderFirstTextView.setText(getArguments().getInt("week")+" неделя");
            mHeaderSecondTextView.setText(Utils.getDayMain(getArguments().getInt("day")));
        }
        else {
            Log.d("LESSONS",String.valueOf(getArguments().getInt("type")) + getArguments().getString("value") +
                    getArguments().getString("date"));
            lessons = mDbReader.readDmain(String.valueOf(getArguments().getInt("type")), getArguments().getString("value"),
                    getArguments().getString("date"));
            mHeaderFirstTextView.setText(Utils.getDayAndMonth(getArguments().getString("date")));
            mHeaderSecondTextView.setText(Utils.getFullDay(getArguments().getString("date")));
        }
        if (lessons.isEmpty()) {
            Log.e("EMPTY","EMPTYY");

            mRecyclerView.setVisibility(View.GONE);
            mEmptyListTextView.setVisibility(View.VISIBLE);
        }
        else {
            mAdapter = new TimetableLessonsAdapter(lessons, getActivity());
            mRecyclerView.setAdapter(mAdapter);
        }
        return view;
    }

    private void animateHeader(){
        mHeaderLayout.setTranslationY(-250);
        mHeaderLayout.animate().translationY(0).setDuration(300).setInterpolator(new AccelerateInterpolator()).start();
    }

}
