package ru.majo.bsutimetable;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import carbon.widget.LinearLayout;

/**
 * Created by Majo on 12.07.2015.
 */
public class TimetableViewAdapter extends RecyclerView.Adapter<TimetableViewAdapter.ViewHolder>{

    private Context mContext;
    private int mLastPosition = -1;

    private String[] mList = {"Очные группы","Преподаватели","Заочные группы","Полная занятость преподавателя"
            ,"Циклы","Полная занятость аудиторий","Сессия"};

    public interface TimetableClickListener{
        public void sendType(int type);
    }
    TimetableClickListener mTimetableClickListener;
    public TimetableViewAdapter(Context context){
        mContext = context;
        mTimetableClickListener = (TimetableClickListener)mContext;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_timetable_view,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.viewTextView.setText(mList[position]);
        holder.abbreviationTextView.setText(String.valueOf(mList[position].charAt(0)));
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTimetableClickListener.sendType(position);
            }
        });
        //holder.colorImageView.setBackgroundColor();
        setAnimation(holder.container, position);
    }

    @Override
    public int getItemCount() {
        return 6;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView viewTextView;
        TextView abbreviationTextView;
        ImageView colorImageView;

        LinearLayout container;
        LinearLayout layout;

        public ViewHolder(View itemView) {
            super(itemView);
            container = (LinearLayout)itemView.findViewById(R.id.layout_container);
            layout = (LinearLayout)itemView.findViewById(R.id.layout_timetable);
            viewTextView = (TextView)itemView.findViewById(R.id.view_timetable);
            abbreviationTextView = (TextView)itemView.findViewById(R.id.abbreviation_view_timetable);
            colorImageView = (ImageView)itemView.findViewById(R.id.color_view_timetable);
        }
    }
    private void setAnimation(View viewToAnimate, int position)
    {
        if (position > mLastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.slide_in_right);
            animation.setDuration((position+1)*100);
            viewToAnimate.startAnimation(animation);
            mLastPosition = position;
        }
    }
}
