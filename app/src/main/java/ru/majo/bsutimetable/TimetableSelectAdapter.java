package ru.majo.bsutimetable;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.zip.Inflater;

import carbon.widget.LinearLayout;
import carbon.widget.TextView;

/**
 * Created by Majo on 14.07.2015.
 */
public class TimetableSelectAdapter extends RecyclerView.Adapter<TimetableSelectAdapter.ViewHolder>{

    private ArrayList<String> mList;
    private int[] mListFavorites;
    private int mType;

    private Context mContext;

    public interface Listener {
        public void getValue(int type, String value);
    }

    private Listener mListener;

    public TimetableSelectAdapter(Context context,int type,ArrayList<String> list){
        mType = type;
        mList = list;
        mListFavorites = Utils.getFavorites(context,list,type);
        mListener = (Listener)context;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_timetable_select,parent,false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mText.setText(mList.get(position));
        if (mListFavorites[position]==1)
            holder.mStarFavorite.setImageResource(R.drawable.ic_action_toggle_star);
        else
            holder.mStarFavorite.setImageResource(R.drawable.ic_action_toggle_star_outline);
        holder.mLayoutStar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListFavorites[position]==1){
                    mListFavorites[position]=0;
                    holder.mStarFavorite.setImageResource(R.drawable.ic_action_toggle_star_outline);
                    Utils.deleteFavoriteInput(mContext,mList.get(position));
                }
                else{
                    mListFavorites[position]=1;
                    holder.mStarFavorite.setImageResource(R.drawable.ic_action_toggle_star);
                    Utils.writeFavoriteInput(mContext,mList.get(position),mType);
                }
            }
        });
        holder.mLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.getValue(mType,mList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private LinearLayout mLayout;
        private LinearLayout mLayoutStar;
        private TextView mText;
        private ImageView mStarFavorite;

        public ViewHolder(View itemView) {
            super(itemView);
            mLayout = (LinearLayout)itemView.findViewById(R.id.select_layout);
            mLayoutStar = (LinearLayout)itemView.findViewById(R.id.star_layout);
            mText = (TextView)itemView.findViewById(R.id.select_text);
            mStarFavorite= (ImageView)itemView.findViewById(R.id.star_favorite_image);
        }
    }
}
