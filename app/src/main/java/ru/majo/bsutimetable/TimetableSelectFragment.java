package ru.majo.bsutimetable;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.melnykov.fab.FloatingActionButton;

import java.util.ArrayList;

import carbon.widget.LinearLayout;

import carbon.widget.RecyclerView;

/**
 * Created by Majo on 14.07.2015.
 */
public class TimetableSelectFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private FloatingActionButton mSearchButton;
    private LinearLayout mSearchLayout;
    private LinearLayout mSelectLayout;
    private EditText mEditText;
    DBReader mDBReader;

    private ArrayList<String> mList,mSortList;

    public static TimetableSelectFragment newInstance(int type){
        TimetableSelectFragment timetableSelectFragment = new TimetableSelectFragment();
        Bundle bundle = new Bundle(1);
        bundle.putInt("type_select", type);
        timetableSelectFragment.setArguments(bundle);
        return timetableSelectFragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_timetable_select,null);
        mRecyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        mSearchLayout = (LinearLayout)view.findViewById(R.id.layout_search_select);
        mSelectLayout = (LinearLayout)view.findViewById(R.id.layout_select);
        mSearchButton = (FloatingActionButton)view.findViewById(R.id.floating_search_select);
        mEditText = (EditText)view.findViewById(R.id.search_edittext);

        mLayoutManager = new LinearLayoutManager(getActivity());

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(new TimetableViewAdapter(getActivity()));

        mList = new ArrayList<String>();
        mSortList = new ArrayList<String>();

        mDBReader = new DBReader(getActivity());
        TextView toolbar = (TextView) getActivity().findViewById(R.id.toolbar_text);
        toolbar.setText("Расписание");
        mRecyclerView.setAdapter(new TimetableSelectAdapter(getActivity(), 0, new ArrayList<String>()));
        new AsyncTask<Integer,Void,ArrayList<String>>(){
            @Override
            protected ArrayList<String> doInBackground(Integer ... params ) {
                return mDBReader.readListForHints(params[0]);
            }
            @Override
            protected void onPostExecute(ArrayList<String> result) {
                super.onPostExecute(result);
                mList = result;
                mSortList.addAll(result);
                if (getActivity()!=null)
                    mRecyclerView.setAdapter(new TimetableSelectAdapter(getActivity(),getArguments().getInt("type_select"),result));
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, getArguments().getInt("type_select"));

        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSearchLayout.isShown()) {
                    animateViewUp(mSearchLayout);
                } else {
                    animateViewDown(mSearchLayout);
                }
            }
        });
        mSearchButton.attachToRecyclerView(mRecyclerView);



        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int textLength = mEditText.getText().length();
                mSortList.clear();
                for (int i = 0; i < mList.size(); i++) {
                    if (textLength <= mList.get(i).length()) {

                        if(mList.get(i).toLowerCase().
                                contains(mEditText.getText().toString().toLowerCase().trim())) {
                            mSortList.add(mList.get(i));
                        }
                    }
                }
                mRecyclerView.setAdapter(new TimetableSelectAdapter(getActivity(),getArguments().getInt("type_select"),mSortList));

            }
        });

        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (getView() == null)
            return;
        View view = getView().findFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void animateViewDown(View view){
        view.setVisibility(View.VISIBLE);
        view.setTranslationY(-100);
        view.animate().translationY(0).setDuration(300).setInterpolator(new AccelerateInterpolator()).start();
    }
    private void animateViewUp(View view){
        view.animate().translationY(-100).setDuration(300).setInterpolator(new AccelerateInterpolator()).start();
        view.setVisibility(View.GONE);
    }
}
