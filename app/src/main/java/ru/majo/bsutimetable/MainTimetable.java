package ru.majo.bsutimetable;

/**
 * Created by Majo on 11.07.2015.
 */
public class MainTimetable {

    private String mTime;
    private String mAuditory;
    private String mType;
    private String mFirstType;
    private String mDiss;

    public String getTime() {
        return mTime;
    }

    public void setTime(String time) {
        mTime = time;
    }

    public String getAuditory() {
        return mAuditory;
    }

    public void setAuditory(String auditory) {
        mAuditory = auditory;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getFirstType() {
        return mFirstType;
    }

    public void setFirstType(String firstType) {
        mFirstType = firstType;
    }

    public String getDiss() {
        return mDiss;
    }

    public void setDiss(String diss) {
        mDiss = diss;
    }

    public void setTimetable(String time, String auditory, String firstType, String type, String diss){
        mTime = time;
        mAuditory = auditory;
        mFirstType = firstType;
        mType = type;
        mDiss = diss;
    }
    public void setTeacherTimetable(String time, String auditory, String type, String diss){
        mTime = time;
        mAuditory = auditory;
        mType = type;
        mDiss = diss;
    }

    @Override
    public String toString() {
        return "MainTimetable{" +
                "mTime='" + mTime + '\'' +
                ", mAuditory='" + mAuditory + '\'' +
                ", mType='" + mType + '\'' +
                ", mFirstType='" + mFirstType + '\'' +
                ", mDiss='" + mDiss + '\'' +
                '}';
    }
}
