package ru.majo.bsutimetable;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.preference.PreferenceFragment;
import android.util.Log;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.jenzz.materialpreference.Preference;
import com.jenzz.materialpreference.SwitchPreference;

import android.preference.Preference.OnPreferenceClickListener;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Majo on 16.07.2015.
 */
public class SettingsFragment extends PreferenceFragment {
    private final static String TAG = "SettingsFragment";

    private DBHelper sqh;
    private SQLiteDatabase sqdb;
    private SharedPreferences mSP;

    private Boolean isAutomatic;

    //private String prefix ="http://www.bsu.ru";
    private String prefix ="http://192.168.56.1:8080";
    private String url1 = prefix + "/content/rasp/rasp_dmain.json";
    private String url2 = prefix + "/content/rasp/rasp_group.json";
    private String url3 = prefix + "/content/rasp/rasp_auditory.json";
    private String url4 = prefix + "/content/rasp/rasp_main.json";
    private String url5 = prefix + "/content/rasp/rasp_subjects.json";
    private String url6 = prefix + "/content/rasp/rasp_teachers.json";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.prefs);
        sqh = new DBHelper(getActivity());
        sqdb = sqh.getWritableDatabase();
        DBReader dbr = new DBReader(getActivity());
        mSP = getActivity().getSharedPreferences("settings", Context.MODE_PRIVATE);
        isAutomatic = mSP.getBoolean("automatic_update",false);
        TextView toolbar = (TextView) getActivity().findViewById(R.id.toolbar_text);
        toolbar.setText("Настройки");

        Preference downloadPreference = (Preference)findPreference("fullupdate");
        downloadPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(android.preference.Preference preference) {
                if (Utils.checkInternet(getActivity())) {
                    new JsonReadTask(getActivity()).execute(new String[]{url1, url2, url3, url4, url5, url6});
                } else {
                    Toast.makeText(getActivity(),"Отсутствует подключение к интернету",Toast.LENGTH_SHORT).show();
                }
                return false;
            }

        });
        Preference changePreference = (Preference)findPreference("changevalue");
        String value = mSP.getString("value_save", new String());
        if (mSP.getInt("type_save", -1)==1 || mSP.getInt("type_save", -1)==3)
            value = dbr.convertbackTeacher(value);
        changePreference.setSummary(value);
        changePreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(android.preference.Preference preference) {
                getFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        ChoiceFragment.newInstance()).addToBackStack(null).commit();
                return false;
            }

        });
        final SwitchPreference autoupdatePreference = (SwitchPreference)findPreference("autoupdate");
        autoupdatePreference.setDefaultValue(isAutomatic);
        autoupdatePreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(android.preference.Preference preference) {
                writeToSettingsAutomaticField(autoupdatePreference.isChecked());
                return false;
            }
        });

        Preference feedbackPreference = (Preference)findPreference("feedback");
        feedbackPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(android.preference.Preference preference) {
                sendFeedBack();
                return false;
            }
        });


        Preference versionPreference = (Preference)findPreference("version");
        versionPreference.setSummary(getVersion());
    }

    private void writeToSettingsAutomaticField(Boolean value){
        SharedPreferences.Editor e =  mSP.edit();
        e.putBoolean("automatic_update", value);
        e.apply();
    }


    private void sendFeedBack(){
        Intent Email = new Intent(Intent.ACTION_SEND);
        Email.setType("text/email");
        Email.putExtra(Intent.EXTRA_EMAIL, new String[]{"esavinkin@gmail.com"});
        Email.putExtra(Intent.EXTRA_SUBJECT, "Feedback");
        Email.putExtra(Intent.EXTRA_TEXT, "");
        startActivity(Intent.createChooser(Email, "Send Feedback:"));
    }

    private String getVersion(){
        PackageManager manager = getActivity().getPackageManager();
        PackageInfo info;
        try {
            info = manager.getPackageInfo(
                    getActivity().getPackageName(), 0);
            String version = info.versionName;
            return version;
        } catch (PackageManager.NameNotFoundException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
        }
        return "";
    }

    public static class JsonReadTask extends AsyncTask<String, String, String> {
        ProgressDialog dialog;
        private DBHelper sqh;
        private SQLiteDatabase sqdb;
        private Context mContext;

        public JsonReadTask(Context context){
            mContext = context;
            sqh = new DBHelper(context);
            sqdb = sqh.getWritableDatabase();
        }

        @Override
        protected void onPreExecute(){
            dialog = new ProgressDialog(mContext);
            dialog.setCancelable(false);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();

            dialog.setContentView(R.layout.dialog_loading);
            sqh.delete(sqdb);
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            InputStream input = null;
            OutputStream output = null;
            Log.i(TAG, "load dmain");
            HttpURLConnection connection = null;
            try{
                try {
                    URL url = new URL(params[0]);
                    connection = (HttpURLConnection) url.openConnection();
                    connection.connect();
                    input = connection.getInputStream();
                    output = new FileOutputStream("/data/data/ru.majo.bsutimetable/rasp_dmain.json");

                    byte data[] = new byte[8192];
                    int count;
                    while ((count = input.read(data)) != -1) {
                        if (isCancelled()) {
                            input.close();
                            return null;
                        }
                        output.write(data, 0, count);
                    }

                } catch (Exception e) {
                    return e.toString();
                }	        if (output != null)
                    try {
                        if (input != null)
                            input.close();
                        output.close();
                        } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }


                File file = new File("/data/data/ru.majo.bsutimetable/rasp_dmain.json");


                JsonFactory jFactory = new JsonFactory();
                JsonParser jParser = jFactory.createJsonParser(file);
                String sql = "INSERT INTO rasp_dmain" +" VALUES (?,?,?,?,?,?,?,?);";
                Log.i("My Logs","4");
                SQLiteStatement statement = sqdb.compileStatement(sql);
                sqdb.beginTransaction();
                while (jParser.nextToken()!= JsonToken.END_ARRAY){
                    statement.clearBindings();
                    while (jParser.nextToken() != JsonToken.END_OBJECT) {
                        String fieldname = jParser.getCurrentName();
                        if ("grup".equals(fieldname))
                        {
                            jParser.nextToken();
                            statement.bindString(1, jParser.getText().replace(" ", ""));
                        }
                        if ("dz".equals(fieldname))
                        {
                            jParser.nextToken();
                            String s=jParser.getText();
                            String string=s.charAt(0)+""+s.charAt(1)+s.charAt(2)+s.charAt(3)+"-"+
                                    s.charAt(4)+s.charAt(5)+"-"+s.charAt(6)+s.charAt(7);
                            statement.bindString(2, string);
                        }
                        if ("para".equals(fieldname))
                        {
                            jParser.nextToken();
                            statement.bindString(3, jParser.getText());
                        }
                        if ("dis".equals(fieldname))
                        {
                            jParser.nextToken();
                            statement.bindString(4, jParser.getText());
                        }
                        if ("vid".equals(fieldname))
                        {
                            jParser.nextToken();
                            statement.bindString(5, jParser.getText());
                        }
                        if ("aud".equals(fieldname))
                        {
                            jParser.nextToken();
                            statement.bindString(6, jParser.getText());
                        }
                        if ("prep".equals(fieldname))
                        {
                            jParser.nextToken();
                            statement.bindString(7, jParser.getText());
                        }
                        if ("tip".equals(fieldname))
                        {
                            jParser.nextToken();
                            statement.bindString(8, jParser.getText());
                        }
                    }
                    statement.execute();
                }
                sqdb.setTransactionSuccessful();
                sqdb.endTransaction();
                jParser.close();
                file.delete();
                try {
                    URL url = new URL(params[1]);
                    connection = (HttpURLConnection) url.openConnection();
                    connection.connect();
                    input = connection.getInputStream();
                    output = new FileOutputStream("/data/data/ru.majo.bsutimetable/rasp_group.json");

                    byte data[] = new byte[8192];
                    int count;
                    while ((count = input.read(data)) != -1) {
                        output.write(data, 0, count);
                    }

                } catch (Exception e) {
                    return e.toString();
                }
                Log.i("My Logs","DADADADADA5");
                if (output != null)
                    try {
                        if (input != null)
                            input.close();
                        output.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                file = new File("/data/data/ru.majo.bsutimetable/rasp_group.json");

                jFactory = new JsonFactory();
                jParser = jFactory.createJsonParser(file);
                sql = "INSERT INTO rasp_group" +" VALUES (?,?);";
                Log.i("My Logs","6");
                statement = sqdb.compileStatement(sql);
                sqdb.beginTransaction();
                while (jParser.nextToken()!= JsonToken.END_ARRAY){
                    statement.clearBindings();
                    while (jParser.nextToken() != JsonToken.END_OBJECT) {
                        String fieldname = jParser.getCurrentName();
                        if ("grup".equals(fieldname))
                        {
                            jParser.nextToken();
                            statement.bindString(1, jParser.getText());
                        }
                        if ("tip".equals(fieldname))
                        {
                            jParser.nextToken();
                            statement.bindString(2, jParser.getText());
                        }

                    }
                    statement.execute();
                }
                sqdb.setTransactionSuccessful();
                sqdb.endTransaction();
                jParser.close();
                file.delete();

                try {
                    URL url = new URL(params[2]);
                    connection = (HttpURLConnection) url.openConnection();
                    connection.connect();
                    input = connection.getInputStream();
                    output = new FileOutputStream("/data/data/ru.majo.bsutimetable/rasp_auditory.json");

                    byte data[] = new byte[8192];
                    int count;
                    while ((count = input.read(data)) != -1) {
                        output.write(data, 0, count);
                    }
                } catch (Exception e) {
                    return e.toString();
                }
                Log.i("My Logs","DADADADADA6");
                if (output != null)
                    try {
                        if (input != null)
                            input.close();
                        output.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                file = new File("/data/data/ru.majo.bsutimetable/rasp_auditory.json");


                jFactory = new JsonFactory();
                jParser = jFactory.createJsonParser(file);
                sql = "INSERT INTO rasp_auditory" +" VALUES (?,?);";
                Log.i("My Logs","5");
                statement = sqdb.compileStatement(sql);
                sqdb.beginTransaction();
                while (jParser.nextToken()!= JsonToken.END_ARRAY){
                    statement.clearBindings();
                    while (jParser.nextToken() != JsonToken.END_OBJECT) {
                        String fieldname = jParser.getCurrentName();
                        if ("kor".equals(fieldname))
                        {
                            jParser.nextToken();
                            statement.bindString(1, jParser.getText());
                        }
                        if ("aud".equals(fieldname))
                        {
                            jParser.nextToken();
                            statement.bindString(2, jParser.getText());
                        }

                    }
                    statement.execute();
                }
                sqdb.setTransactionSuccessful();
                sqdb.endTransaction();
                jParser.close();
                file.delete();
            } catch (JsonParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            input = null;
            output = null;
            connection = null;
            try{
                try {
                    URL url = new URL(params[3]);
                    connection = (HttpURLConnection) url.openConnection();
                    connection.connect();
                    input = connection.getInputStream();
                    output = new FileOutputStream("/data/data/ru.majo.bsutimetable/rasp_main.json");

                    byte data[] = new byte[8192];
                    int count;
                    while ((count = input.read(data)) != -1) {
                        if (isCancelled()) {
                            input.close();
                            return null;
                        }
                        output.write(data, 0, count);
                    }

                } catch (Exception e) {
                    return e.toString();
                }
                Log.i("My Logs","DADADADADA");
                if (output != null)
                    try {
                        if (input != null)
                            input.close();
                        output.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                File file = new File("/data/data/ru.majo.bsutimetable/rasp_main.json");


                JsonFactory jFactory = new JsonFactory();
                JsonParser jParser = jFactory.createParser(file);//createJsonParser(jsonResult);
                String sql = "INSERT INTO rasp_main" +" VALUES (?,?,?,?,?,?,?,?);";
                Log.i("My Logs","2");
                SQLiteStatement statement = sqdb.compileStatement(sql);
                sqdb.beginTransaction();
                while (jParser.nextToken()!= JsonToken.END_ARRAY){
                    statement.clearBindings();
                    while (jParser.nextToken() != JsonToken.END_OBJECT) {
                        String fieldname = jParser.getCurrentName();
                        if ("subject_id".equals(fieldname))
                        {
                            jParser.nextToken();
                            statement.bindString(1, jParser.getText());
                        }
                        if ("teacher_id".equals(fieldname))
                        {
                            jParser.nextToken();
                            statement.bindString(2, jParser.getText());
                        }
                        if ("auditory".equals(fieldname))
                        {
                            jParser.nextToken();
                            statement.bindString(3, jParser.getText());
                        }
                        if ("pair_type".equals(fieldname))
                        {
                            jParser.nextToken();
                            statement.bindString(4, jParser.getText());
                        }
                        if ("pair_number".equals(fieldname))
                        {
                            jParser.nextToken();
                            statement.bindString(5, jParser.getText());
                        }
                        if ("group_number".equals(fieldname))
                        {
                            jParser.nextToken();
                            statement.bindString(6, jParser.getText().replaceAll("[\\s]{1,}", ""));
                        }
                        if ("day_of_week".equals(fieldname))
                        {
                            jParser.nextToken();
                            statement.bindString(7, jParser.getText());
                        }
                        if ("week".equals(fieldname))
                        {
                            jParser.nextToken();
                            statement.bindString(8, jParser.getText());
                        }
                    }
                    publishProgress();
                    statement.execute();
                }
                sqdb.setTransactionSuccessful();
                sqdb.endTransaction();
                jParser.close();
                file.delete();
                try {
                    URL url = new URL(params[4]);
                    connection = (HttpURLConnection) url.openConnection();
                    connection.connect();
                    input = connection.getInputStream();
                    output = new FileOutputStream("/data/data/ru.majo.bsutimetable/rasp_subjects.json");

                    Log.i("My Logs","START2");
                    byte data[] = new byte[8192];
                    int count;
                    while ((count = input.read(data)) != -1) {
                        if (isCancelled()) {
                            input.close();
                            return null;
                        }
                        output.write(data, 0, count);
                    }

                } catch (Exception e) {
                    return e.toString();
                }
                if (output != null)
                    try {
                        if (input != null)
                            input.close();
                        output.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                file = new File("/data/data/ru.majo.bsutimetable/rasp_subjects.json");



                jFactory = new JsonFactory();
                jParser = jFactory.createParser(file);
                sql = "INSERT INTO rasp_subjects" +" VALUES (?,?,?);";
                Log.i("My Logs","3");
                statement = sqdb.compileStatement(sql);
                sqdb.beginTransaction();
                while (jParser.nextToken()!= JsonToken.END_ARRAY){
                    statement.clearBindings();
                    while (jParser.nextToken() != JsonToken.END_OBJECT) {
                        String fieldname = jParser.getCurrentName();
                        if ("dis".equals(fieldname))
                        {
                            jParser.nextToken();
                            statement.bindString(1, jParser.getText());
                        }
                        if ("diss".equals(fieldname))
                        {
                            jParser.nextToken();
                            statement.bindString(2, jParser.getText().replaceAll("[\\s]{2,}", ""));
                        }
                        if ("disp".equals(fieldname))
                        {
                            jParser.nextToken();
                            statement.bindString(3, jParser.getText().replaceAll("[\\s]{2,}", ""));
                        }

                    }
                    statement.execute();
                }
                sqdb.setTransactionSuccessful();
                sqdb.endTransaction();
                jParser.close();
                file.delete();

                try {
                    URL url = new URL(params[5]);
                    connection = (HttpURLConnection) url.openConnection();
                    connection.connect();
                    input = connection.getInputStream();
                    output = new FileOutputStream("/data/data/ru.majo.bsutimetable/rasp_teachers.json");

                    Log.i("My Logs","START3");
                    byte data[] = new byte[8192];
                    int count;
                    while ((count = input.read(data)) != -1) {
                        output.write(data, 0, count);
                    }

                } catch (Exception e) {
                    return e.toString();
                }
                if (output != null)
                    try {
                        if (input != null)
                            input.close();
                        output.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                file = new File("/data/data/ru.majo.bsutimetable/rasp_teachers.json");


                jFactory = new JsonFactory();
                jParser = jFactory.createParser(file);
                sql = "INSERT INTO rasp_teachers" +" VALUES (?,?);";
                Log.i("My Logs","4");
                statement = sqdb.compileStatement(sql);
                sqdb.beginTransaction();
                while (jParser.nextToken()!= JsonToken.END_ARRAY){
                    statement.clearBindings();
                    while (jParser.nextToken() != JsonToken.END_OBJECT) {
                        String fieldname = jParser.getCurrentName();
                        if ("prep".equals(fieldname))
                        {
                            jParser.nextToken();
                            statement.bindString(1, jParser.getText());
                        }
                        if ("fio".equals(fieldname))
                        {
                            jParser.nextToken();
                            statement.bindString(2, jParser.getText().replaceAll("[\\s]{2,}", ""));
                        }

                    }
                    statement.execute();
                }
                sqdb.setTransactionSuccessful();
                sqdb.endTransaction();
                jParser.close();
                file.delete();
            }catch (JsonParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            SharedPreferences mSP = mContext.getSharedPreferences("settings", Context.MODE_PRIVATE);
            SharedPreferences.Editor e =  mSP.edit();
            e.putString("automatic_update_date", Utils.newDate());
            e.commit();
            dialog.dismiss();
        }
    }
}
