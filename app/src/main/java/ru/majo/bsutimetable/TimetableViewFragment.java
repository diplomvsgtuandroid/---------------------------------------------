package ru.majo.bsutimetable;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import carbon.widget.RecyclerView;

/**
 * Created by Majo on 12.07.2015.
 */
public class TimetableViewFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_timetable,null);
        mRecyclerView = (RecyclerView)view.findViewById(R.id.view_recyclerview);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(new TimetableViewAdapter(getActivity()));
        TextView toolbar = (TextView) getActivity().findViewById(R.id.toolbar_text);
        toolbar.setText("Расписание");
        return view;
    }
}
