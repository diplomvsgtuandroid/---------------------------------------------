package ru.majo.bsutimetable;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import carbon.widget.LinearLayout;
import carbon.widget.TextView;

/**
 * Created by Majo on 17.07.2015.
 */
public class TimetableFavoriteAdapter extends RecyclerView.Adapter<TimetableFavoriteAdapter.ViewHolder> {

    private ArrayList<FavoriteTimetable> mList = new ArrayList<FavoriteTimetable>();
    private Listener mListester;
    public TimetableFavoriteAdapter(Context context,ArrayList<FavoriteTimetable> list){
        mList = list;
        mListester = (Listener)context;
    }

    public interface Listener {
        public void getValue(int type, String value);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_timetable_favorite,parent,false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.textFavorite.setText(mList.get(position).getText());
        holder.typeFavorite.setText(Utils.getType(mList.get(position).getType()));
        holder.abbrFavorite.setText(String.valueOf(Utils.getType(mList.get(position).getType()).charAt(0)));
        holder.mLayoutFavorites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListester.getValue(mList.get(position).getType(),mList.get(position).getText());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout mLayoutFavorites;
        private TextView textFavorite;
        private TextView typeFavorite;
        private TextView abbrFavorite;

        public ViewHolder(View itemView) {
            super(itemView);
            mLayoutFavorites = (LinearLayout)itemView.findViewById(R.id.layout_favorite);
            textFavorite = (TextView)itemView.findViewById(R.id.select_favorite_timetable);
            typeFavorite = (TextView)itemView.findViewById(R.id.type_favorite_timetable);
            abbrFavorite = (TextView)itemView.findViewById(R.id.abbreviation_favorite_timetable);

        }
    }
}
