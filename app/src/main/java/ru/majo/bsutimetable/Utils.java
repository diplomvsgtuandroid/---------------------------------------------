package ru.majo.bsutimetable;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Majo on 23.07.2015.
 */
public class Utils {

    public static boolean isHaveMonth(int month,ArrayList<String> dates){
        DateFormat dateFormat  = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        for (int i=0;i<dates.size();i++){
            try {
                cal.setTime(dateFormat.parse(dates.get(i)));
                if (month==(cal.get(Calendar.MONTH)+1)){
                    return true;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public static String getDay(int i){
        switch (i){
            case 1:return "Воскресенье";
            case 2:return "Понедельник";
            case 3:return "Вторник";
            case 4:return "Среда";
            case 5:return "Четверг";
            case 6:return "Пятница";
            default:return "Суббота";

        }
    }

    public static String getShortDay(int i){
        switch (i){
            case 0:return "Пн";
            case 1:return "Вт";
            case 2:return "Ср";
            case 3:return "Чт";
            case 4:return "Пт";
            default:return "Сб";
        }
    }

    public static String getDayMain(int i){
        switch (i){
            case 1:return "Понедельник";
            case 2:return "Вторник";
            case 3:return "Среда";
            case 4:return "Четверг";
            case 5:return "Пятница";
            case 6:return "Суббота";
            default:return "Воскресенье";


        }
    }


    public static String getMonth(int i){
        switch (i){
            case 1:return "Январь";
            case 2:return "Февраль";
            case 3:return "Март";
            case 4:return "Апрель";
            case 5:return "Май";
            case 6:return "Июнь";
            case 7:return "Июль";
            case 8:return "Август";
            case 9:return "Сентябрь";
            case 10:return "Октябрь";
            case 11:return "Ноябрь";
            default:return "Декабрь";
        }
    }

    public static String getMonthOthers(int i){
        switch (i){
            case 1:return "Января";
            case 2:return "Февраля";
            case 3:return "Марта";
            case 4:return "Апреля";
            case 5:return "Мая";
            case 6:return "Июня";
            case 7:return "Июля";
            case 8:return "Августа";
            case 9:return "Сентября";
            case 10:return "Октября";
            case 11:return "Ноября";
            default:return "Декабря";
        }
    }


    public static int[] getFavorites(Context context, ArrayList<String> array, int type){

        DBReader dbReader = new DBReader(context);
        String[] favorites = dbReader.readFavoriteInput(type);
        int[] result = new int[array.size()];
        for (int i=0;i<favorites.length;i++)
            for (int j=0;j<array.size();j++){
                if (array.get(j).equals(favorites[i])) {
                    result[j]=1;
                    break;
                }
            }
        return result;
    }

    public static boolean checkInternet(Context context){
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        NetworkInfo niw = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (ni == null)
            return false;
        else if (niw.isConnected() || ni!=null)
            return true;
        else
            return false;

    }

    public static  ArrayList<FavoriteTimetable> readFavorite(Context context){
        DBHelper sqh = new DBHelper(context);
        SQLiteDatabase sqdb = sqh.getWritableDatabase();
        ArrayList<FavoriteTimetable> data = new ArrayList<FavoriteTimetable>();
        String query = "SELECT * FROM " + DBHelper.TABLE_SAVED;
        Cursor cursor = sqdb.rawQuery(query, null);
        FavoriteTimetable favoriteTimetable;
        while (cursor.moveToNext()){
            favoriteTimetable = new FavoriteTimetable();
            favoriteTimetable.setText(cursor.getString(cursor.getColumnIndex(DBHelper.SAVED_INPUT)));
            favoriteTimetable.setType(Integer.parseInt(cursor.getString(cursor.getColumnIndex(DBHelper.SAVED_TYPE))));
            data.add(favoriteTimetable);
        }
        return data;
    }

    public static void writeFavoriteInput(Context context,String input,int type){
        DBHelper sqh = new DBHelper(context);
        SQLiteDatabase sqdb = sqh.getWritableDatabase();
        ContentValues newValues = new ContentValues();
        newValues.put(DBHelper.SAVED_INPUT, input);
        newValues.put(DBHelper.SAVED_TYPE, type);
        sqdb.insert(DBHelper.TABLE_SAVED, null, newValues);
    }
    public static void deleteFavoriteInput(Context context,String input){
        DBHelper sqh = new DBHelper(context);
        SQLiteDatabase sqdb = sqh.getWritableDatabase();
        sqdb.delete(DBHelper.TABLE_SAVED, DBHelper.SAVED_INPUT + " = '" + input + "'", null);
    }

    public static String getType(int type){
        switch (type){
            case 0:return "Очные группы";
            case 1:return "Преподаватели";
            case 2:return "Заочные группы";
            case 3:return "Полная занятость преподавателей";
            case 4:return "Циклы";
            default:return "Полная занятость аудиторий";
        }
    }

    public static ArrayList<String> getList(){
        ArrayList<String> list = new ArrayList<String>();
        for (int i=0;i<5;i++)
            list.add(getType(i));
        return list;
    }

    public static String newDate(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format( Calendar.getInstance().getTime());
    }

    public static String getDayAndMonth(String date){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String returnValue = "";
        try {
            Date newDate = dateFormat.parse(date);
            Calendar cal = Calendar.getInstance();
            cal.setTime(newDate);
            returnValue = cal.get(Calendar.DAY_OF_MONTH) + " " + getMonthOthers(cal.get(Calendar.MONTH) + 1);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return returnValue;
    }

    public static String getFullDay(String date){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String returnValue = "";
        try {
            Date newDate = dateFormat.parse(date);
            Calendar cal = Calendar.getInstance();
            cal.setTime(newDate);
            returnValue = getDay(cal.get(Calendar.DAY_OF_WEEK));

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return returnValue;
    }

    public static int getParity(String dz){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String nachalo="2015-09-01";
        int startcounter=1;
        String curentday=dz;//dateFormat.format(new Date());
        Date date1,date2;
        try {
            date1 = dateFormat.parse(nachalo);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date1);
            date2 = dateFormat.parse(curentday);
            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(date2);
            cal1.getTime();
            while(cal.getTime().before(cal1.getTime()))
            {
                startcounter+=1;
                cal.add(Calendar.DATE, 7);
            }
            int week1 = cal.get(Calendar.DAY_OF_WEEK)-1;
            if (week1==0)
                week1=7;
            int week2 = cal1.get(Calendar.DAY_OF_WEEK)-1;
            if (week2==0)
                week2=7;
            if (week1<week2)
                startcounter-=1;

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if(startcounter%2==0)
            return 2;
        else
            return 1;
    }

    public static int getToday(){
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int day=cal.get(Calendar.DAY_OF_WEEK)-1;
        if (day==0)
            day=7;
        return day;
    }
    public static int getTodayOfMonth(){
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int day=cal.get(Calendar.DAY_OF_MONTH);
        return day;
    }

    public static boolean isWeekDifference(String date){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date updateDate = dateFormat.parse(date);
            Date todayDate = new Date();
            long diffInMillies = todayDate.getTime() - updateDate.getTime();
            Log.e("DIFFERENCE", String.valueOf(TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS)));
            if (TimeUnit.DAYS.convert(diffInMillies,TimeUnit.MILLISECONDS)>=7)
                return true;
            else
                return false;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean isDatabaseCorrupted(Context context){
        DBReader dbr = new DBReader(context);
        if (dbr.readCountDmain()==0 || dbr.readCountMain()==0)
            return true;
        else
            return false;
    }
}
