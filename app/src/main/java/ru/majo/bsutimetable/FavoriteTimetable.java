package ru.majo.bsutimetable;

/**
 * Created by Majo on 17.07.2015.
 */
public class FavoriteTimetable {
    private int mType;
    private String mText;

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        mText = text;
    }

    public int getType() {
        return mType;
    }

    public void setType(int type) {
        mType = type;
    }
}
