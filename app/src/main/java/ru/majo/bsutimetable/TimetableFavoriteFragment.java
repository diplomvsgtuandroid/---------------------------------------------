package ru.majo.bsutimetable;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import carbon.widget.RecyclerView;

/**
 * Created by Majo on 17.07.2015.
 */
public class TimetableFavoriteFragment extends Fragment{
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private android.support.v7.widget.RecyclerView.Adapter mAdapter;
    private ArrayList<FavoriteTimetable> mList;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_timetable_favorite,container,false);
        mRecyclerView = (RecyclerView)view.findViewById(R.id.favorite_recyclerview);
        mLayoutManager = new LinearLayoutManager(getActivity());

        mList = Utils.readFavorite(getActivity());
        for (int i=0;i<mList.size();i++){
            Log.i("FAVORITES", mList.get(i).getText());
        }

        TextView toolbar = (TextView) getActivity().findViewById(R.id.toolbar_text);
        toolbar.setText("Избранное");

        mAdapter = new TimetableFavoriteAdapter(getActivity(),mList);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        ItemTouchHelper swipeToDismissTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(android.support.v7.widget.RecyclerView recyclerView, android.support.v7.widget.RecyclerView.ViewHolder viewHolder, android.support.v7.widget.RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                Utils.deleteFavoriteInput(getActivity(),mList.get(viewHolder.getAdapterPosition()).getText());
                mList.remove(viewHolder.getAdapterPosition());
                mAdapter.notifyItemRemoved(viewHolder.getAdapterPosition());
            }
        });
        swipeToDismissTouchHelper.attachToRecyclerView(mRecyclerView);
        return view;
    }
}
