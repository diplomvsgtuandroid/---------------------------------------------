package ru.majo.bsutimetable;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;


/**
 * Created by Majo on 04.07.2015.
 */
public class TimetableDaysAdapter extends RecyclerView.Adapter<TimetableDaysAdapter.ViewHolder>{

    private int mWeek;
    private int mType;
    private String mValue;
    private ArrayList<String> mDays;

    private Context mContext;
    private Listener mListener;
    private int mLastPosition = -1;

    private boolean mIsCurrent = false;
    public interface Listener {
        public void getDayMain(int type, String value,int week,int day);
        public void getDayDMain(int type, String value,String date);
    }
    public TimetableDaysAdapter(Context context, int type, String value, int week, boolean isCurrent){
        mType = type;
        mWeek = week;
        mValue = value;
        mDays = getWeek();
        mContext = context;
        mIsCurrent = isCurrent;
    }

    public TimetableDaysAdapter(Context context,int type, String value, ArrayList<String> days, boolean isCurrent){
        mType = type;
        mDays = days;
        mValue = value;
        mContext = context;
        mIsCurrent = isCurrent;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout itemLayout;
        TextView todayTextView;
        TextView dayTextView;
        TextView abbrTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            itemLayout  = (LinearLayout)itemView.findViewById(R.id.days_item);
            todayTextView = (TextView)itemView.findViewById(R.id.today_textview);
            dayTextView = (TextView)itemView.findViewById(R.id.days_textview);
            abbrTextView = (TextView)itemView.findViewById(R.id.abbreviation_textview);
        }
    }

        @Override
    public TimetableDaysAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        try {
            mListener = (Listener) mContext;
        } catch (ClassCastException e) {
            throw new ClassCastException(mContext.toString() + " must implement onSomeEventListener");
        }
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_timetable_days,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TimetableDaysAdapter.ViewHolder holder, final int position) {
        Log.i("Position", Integer.toString(position));
        if (mType<2) {
            holder.abbrTextView.setText(String.valueOf(mDays.get(position).charAt(0)));
            holder.dayTextView.setText(mDays.get(position));
            holder.itemLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.getDayMain(mType, mValue, mWeek, position);
                }
            });
            if (position+1==Utils.getToday() && mIsCurrent)
                holder.todayTextView.setVisibility(View.VISIBLE);
            else
                holder.todayTextView.setVisibility(View.INVISIBLE);
        }
        else {
            holder.abbrTextView.setText(String.valueOf(getDayNumber(mDays.get(position))));
            holder.dayTextView.setText(getDayOfWeek(mDays.get(position)));
            holder.itemLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.getDayDMain(mType, mValue, mDays.get(position));
                }
            });
            if (Utils.getTodayOfMonth()==getDayNumber(mDays.get(position)) && mIsCurrent)
                holder.todayTextView.setVisibility(View.VISIBLE);
            else
                holder.todayTextView.setVisibility(View.INVISIBLE);

        }
    }


    @Override
    public int getItemCount() {
        return mDays.size();
    }

    private ArrayList<String> getWeek(){
        ArrayList<String> week = new ArrayList<String>();
        for (int i=0;i<6;i++){
            week.add(getDays(i));
        }
        return week;
    }

    private int getDayNumber(String date){
        DateFormat dateFormat  = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(dateFormat.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return cal.get(Calendar.DAY_OF_MONTH);
    }

    private String getDayOfWeek(String date){
        DateFormat dateFormat  = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(dateFormat.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.i("DATEDAY",date + " " + cal.get(Calendar.DAY_OF_WEEK));
        int day = cal.get(Calendar.DAY_OF_WEEK)-2;
        if (day<0)
            day = 6;
        return getDays(day);
    }

    private String getDays(int position){
        switch (position){
            case 0:return "Понедельник";
            case 1:return "Вторник";
            case 2:return "Среда";
            case 3:return "Четверг";
            case 4:return "Пятница";
            case 5:return "Суббота";
            default:return "Воскресенье";
        }
    }
}
