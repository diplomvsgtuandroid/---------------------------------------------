## Libraries
* RxJava - https://github.com/ReactiveX/RxJava
* RxAndroid - https://github.com/ReactiveX/RxAndroid
* Dagger 2 - https://github.com/google/dagger
* OkHttp - https://github.com/square/okhttp
* Butter Knife - https://github.com/JakeWharton/butterknife
* Support Preference Fragment - https://github.com/kolavar/android-support-v4-preferencefragment
* Jackson - https://github.com/FasterXML/jackson
* Material Dialog - https://github.com/drakeet/MaterialDialog
* Material CircularProgressView - https://github.com/rahatarmanahmed/CircularProgressView
* Material Preference - https://github.com/jenzz/Android-MaterialPreference
* Floating Action Button - https://github.com/makovkastar/FloatingActionButton